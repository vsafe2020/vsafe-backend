using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
//using VSafe.API.Helpers;
using VSafe.Dto;
using VSafe.Service;
using VSafe.Domain.Entities;
using VSafe.Utility;
using Microsoft.Extensions.Logging;
using IdentityServer4.AccessTokenValidation;
using VSafe.Domain;

namespace VSafe.API.Controllers
{
    /// <summary>
    /// Department endpoint
    /// </summary>
    [Route("api/departments")]
    [Produces("application/json")]
    [Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]
    [ApiController]
    public class DepartmentController : Controller
    {

        #region PRIVATE MEMBERS

        private readonly IDepartmentService _departmentService;
        private ILogger<DepartmentController> _logger;
        private readonly IUrlHelper _urlHelper;
        private readonly IUserInfoService _userInfoService;

        #endregion


        #region CONSTRUCTOR

        public DepartmentController(IDepartmentService departmentService, ILogger<DepartmentController> logger,
            IUserInfoService userInfoService,
            IUrlHelper urlHelper)
        {
            _logger = logger;
            _departmentService = departmentService;
            _urlHelper = urlHelper;
            _userInfoService = userInfoService ?? throw new ArgumentNullException(nameof(userInfoService));
        }

        #endregion


        #region HTTPGET

        [HttpGet(Name = "GetFilteredDepartments")]
        [Produces("application/vnd.tourmanagement.departments.hateoas+json", "application/json")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<List<DepartmentDto>>> GetFilteredDepartments([FromQuery]FilterOptionsModel filterOptionsModel, [FromHeader(Name = "Accept")] string mediaType)
        {

            //if order by fields are not valid.
            if (!_departmentService.ValidMappingExists(filterOptionsModel.OrderBy))
            {
                //then return bad request.
                return BadRequest();
            }

            //if fields are not valid.
            if (!VSafeUtils.TypeHasProperties<DepartmentDto>(filterOptionsModel.Fields))
            {
                //then return bad request.
                return BadRequest();
            }

            //get the paged/filtered show from db. 
            var departmentsFromRepo = await _departmentService.GetFilteredEntities(filterOptionsModel);

            //if HATEOAS links are required.
            if (mediaType == "application/vnd.tourmanagement.departments.hateoas+json")
            {
                //create HATEOAS links for each show.
                departmentsFromRepo.ForEach(department =>
                {
                    var entityLinks = CreateLinksForDepartment(department.Id, filterOptionsModel.Fields);
                    department.links = entityLinks;
                });

                //prepare pagination metadata.
                var paginationMetadata = new
                {
                    totalCount = departmentsFromRepo.TotalCount,
                    pageSize = departmentsFromRepo.PageSize,
                    currentPage = departmentsFromRepo.CurrentPage,
                    totalPages = departmentsFromRepo.TotalPages,
                };

                //add pagination meta data to response header.
                Response.Headers.Add("X-Pagination", JsonConvert.SerializeObject(paginationMetadata));

                //create links for shows.
                var links = CreateLinksForDepartments(filterOptionsModel, departmentsFromRepo.HasNext, departmentsFromRepo.HasPrevious);

                //prepare model with data and HATEOAS links.
                var linkedCollectionResource = new
                {
                    value = departmentsFromRepo,
                    links = links
                };

                //return the data with Ok response.
                return Ok(linkedCollectionResource);
            }
            else
            {
                var previousPageLink = departmentsFromRepo.HasPrevious ?
                    CreateDepartmentsResourceUri(filterOptionsModel, ResourceUriType.PreviousPage) : null;

                var nextPageLink = departmentsFromRepo.HasNext ?
                    CreateDepartmentsResourceUri(filterOptionsModel, ResourceUriType.NextPage) : null;

                //prepare the pagination metadata.
                var paginationMetadata = new
                {
                    previousPageLink = previousPageLink,
                    nextPageLink = nextPageLink,
                    totalCount = departmentsFromRepo.TotalCount,
                    pageSize = departmentsFromRepo.PageSize,
                    currentPage = departmentsFromRepo.CurrentPage,
                    totalPages = departmentsFromRepo.TotalPages
                };

                //add pagination meta data to response header.
                Response.Headers.Add("X-Pagination", JsonConvert.SerializeObject(paginationMetadata));

                //return the data with Ok response.
                return Ok(departmentsFromRepo);
            }
        }


        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [Produces("application/vnd.tourmanagement.departments.hateoas+json", "application/json")]
        [HttpGet("{id}", Name = "GetDepartment")]
        public async Task<ActionResult<Department>> GetDepartment(Guid id, [FromQuery] string fields, [FromHeader(Name = "Accept")] string mediaType)
        {
            object departmentEntity = null;
            object linkedResourceToReturn = null;

            //if fields are not passed.
            if (string.IsNullOrEmpty(fields))
            {
                _logger.LogInformation("GetDepartment called");

                //then get the whole entity and map it to the Dto.
                departmentEntity = Mapper.Map<DepartmentDto>(await _departmentService.GetEntityByIdAsync(id));
            }
            else
            {
                //if fields are passed then get the partial show.
                departmentEntity = await _departmentService.GetPartialEntityAsync(id, fields);
            }

            //if department not found.
            if (departmentEntity == null)
            {
                //then return not found response.
                return NotFound();
            }

            if (mediaType == "application/vnd.tourmanagement.departments.hateoas+json")
            {
                //create HATEOS links
                var links = CreateLinksForDepartment(id, fields);

                //if fields are not passed.
                if (string.IsNullOrEmpty(fields))
                {
                    //convert the typed object to expando object.
                    linkedResourceToReturn = ((DepartmentDto)departmentEntity).ShapeData("") as IDictionary<string, object>;

                    //add the HATEOAS links to the model.
                    ((IDictionary<string, object>)linkedResourceToReturn).Add("links", links);
                }
                else
                {
                    linkedResourceToReturn = departmentEntity;

                    //add the HATEOAS links to the model.
                    ((dynamic)linkedResourceToReturn).links = links;

                }
            }
            else
            {
                linkedResourceToReturn = departmentEntity;
            }

            //return the Ok response.
            return Ok(linkedResourceToReturn);
        }

        #endregion


        #region HTTPPUT

        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpPut("{id}", Name = "UpdateDepartment")]
        public async Task<IActionResult> UpdateDepartment(Guid id, [FromBody]DepartmentForUpdate DepartmentForUpdate)
        {

            //if show not found
            if (!await _departmentService.ExistAsync(x => x.Id == id))
            {
                //then return not found response.
                return NotFound();
            }

            //Update an entity.
            await _departmentService.UpdateEntityAsync(id, DepartmentForUpdate);

            //return the response.
            return NoContent();
        }

        #endregion


        #region HTTPPATCH

        [Consumes("application/json-patch+json")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpPatch("{id}")]
        public async Task<IActionResult> PartiallyUpdateDepartment(Guid id, [FromBody] JsonPatchDocument<DepartmentForUpdate> jsonPatchDocument)
        {
            DepartmentForUpdate dto = new DepartmentForUpdate();
            Department department = new Department();

            //apply the patch changes to the dto. 
            jsonPatchDocument.ApplyTo(dto, ModelState);

            //if the jsonPatchDocument is not valid.
            if (!ModelState.IsValid)
            {
                //then return unprocessableEntity response.
                return new UnprocessableEntityObjectResult(ModelState);
            }

            //if the dto model is not valid after applying changes.
            if (!TryValidateModel(dto))
            {
                //then return unprocessableEntity response.
                return new UnprocessableEntityObjectResult(ModelState);
            }

            //map the chnages from dto to entity.
            Mapper.Map(dto, department);

            //set the Id for the show model.
            department.Id = id;

            //partially update the chnages to the db. 
            await _departmentService.UpdatePartialEntityAsync(department, jsonPatchDocument);

            //return the response.
            return NoContent();
        }

        #endregion


        #region HTTPPOST

        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpPost(Name = "CreateDepartment")]
        public async Task<ActionResult<DepartmentDto>> CreateDepartment([FromBody]DepartmentForCreation department)
        {
            department.AccountId = _userInfoService.AccountId;
            //create a show in db.
            var departmentToReturn = await _departmentService.CreateEntityAsync<DepartmentDto, DepartmentForCreation>(department);

            //return the show created response.
            return CreatedAtRoute("GetDepartment", new { id = departmentToReturn.Id }, departmentToReturn);
        }

        #endregion


        #region HTTPDELETE
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpDelete("{id}", Name = "DeleteDepartmentById")]
        public async Task<IActionResult> DeleteDepartmentById(Guid id)
        {
            //if the department exists
            if (await _departmentService.ExistAsync(x => x.Id == id))
            {
                //delete the department from the db.
                await _departmentService.DeleteEntityAsync(id);
            }
            else
            {
                //if department doesn't exists then returns not found.
                return NotFound();
            }

            //return the response.
            return NoContent();
        }

        #endregion


        #region PRIVATE METHODS

        private IEnumerable<LinkDto> CreateLinksForDepartment(Guid id, string fields)
        {
            var links = new List<LinkDto>();

            if (string.IsNullOrWhiteSpace(fields))
            {
                links.Add(
                  new LinkDto(_urlHelper.Link("GetDepartment", new { id = id }),
                  "self",
                  "GET"));
            }
            else
            {
                links.Add(
                  new LinkDto(_urlHelper.Link("GetDepartment", new { id = id, fields = fields }),
                  "self",
                  "GET"));
            }

            links.Add(
              new LinkDto(_urlHelper.Link("DeleteDepartmentById", new { id = id }),
              "delete_department",
              "DELETE"));

            links.Add(
             new LinkDto(_urlHelper.Link("UpdateDepartment", new { id = id }),
             "update_department",
             "PUT"));

            links.Add(
              new LinkDto(_urlHelper.Link("CreateDepartment", new { }),
              "create_department",
              "POST"));

            //links.Add(
            //   new LinkDto(_urlHelper.Link("GetShowsForTour", new { }),
            //   "shows",
            //   "GET"));

            return links;
        }

        private IEnumerable<LinkDto> CreateLinksForDepartments(FilterOptionsModel filterOptionsModel, bool hasNext, bool hasPrevious)
        {
            var links = new List<LinkDto>();

            links.Add(
               new LinkDto(CreateDepartmentsResourceUri(filterOptionsModel, ResourceUriType.Current), "self", "GET"));

            if (hasNext)
            {
                links.Add(
                  new LinkDto(CreateDepartmentsResourceUri(filterOptionsModel, ResourceUriType.NextPage), "nextPage", "GET"));
            }

            if (hasPrevious)
            {
                links.Add(new LinkDto(CreateDepartmentsResourceUri(filterOptionsModel, ResourceUriType.PreviousPage), "previousPage", "GET"));
            }

            return links;
        }

        private string CreateDepartmentsResourceUri(FilterOptionsModel filterOptionsModel, ResourceUriType type)
        {
            switch (type)
            {
                case ResourceUriType.PreviousPage:
                    return _urlHelper.Link("GetFilteredDepartments",
                      new
                      {
                          fields = filterOptionsModel.Fields,
                          orderBy = filterOptionsModel.OrderBy,
                          searchQuery = filterOptionsModel.SearchQuery,
                          pageNumber = filterOptionsModel.PageNumber - 1,
                          pageSize = filterOptionsModel.PageSize
                      });
                case ResourceUriType.NextPage:
                    return _urlHelper.Link("GetFilteredDepartments",
                      new
                      {
                          fields = filterOptionsModel.Fields,
                          orderBy = filterOptionsModel.OrderBy,
                          searchQuery = filterOptionsModel.SearchQuery,
                          pageNumber = filterOptionsModel.PageNumber + 1,
                          pageSize = filterOptionsModel.PageSize
                      });

                default:
                    return _urlHelper.Link("GetFilteredDepartments",
                    new
                    {
                        fields = filterOptionsModel.Fields,
                        orderBy = filterOptionsModel.OrderBy,
                        searchQuery = filterOptionsModel.SearchQuery,
                        pageNumber = filterOptionsModel.PageNumber,
                        pageSize = filterOptionsModel.PageSize
                    });
            }
        }

        #endregion

    }
}
