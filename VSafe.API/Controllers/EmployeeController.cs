using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
//using VSafe.API.Helpers;
using VSafe.Dto;
using VSafe.Service;
using VSafe.Domain.Entities;
using VSafe.Utility;
using Microsoft.Extensions.Logging;
using IdentityServer4.AccessTokenValidation;

namespace VSafe.API.Controllers
{
    /// <summary>
    /// Employee endpoint
    /// </summary>
    [Route("api/employees")]
    [Produces("application/json")]
    [Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]
    [ApiController]
    public class EmployeeController : Controller
    {

        #region PRIVATE MEMBERS

        private readonly IEmployeeService _employeeService;
        private ILogger<EmployeeController> _logger;
        private readonly IUrlHelper _urlHelper;

        #endregion


        #region CONSTRUCTOR

        public EmployeeController(IEmployeeService employeeService, ILogger<EmployeeController> logger, IUrlHelper urlHelper) 
        {
            _logger = logger;
            _employeeService = employeeService;
            _urlHelper = urlHelper;
        }

        #endregion


        #region HTTPGET

        [HttpGet(Name = "GetFilteredEmployees")]
        [Produces("application/vnd.tourmanagement.employees.hateoas+json", "application/json")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<List<EmployeeDto>>> GetFilteredEmployees([FromQuery]FilterOptionsModel filterOptionsModel, [FromHeader(Name = "Accept")] string mediaType)
        {

            //if order by fields are not valid.
            if (!_employeeService.ValidMappingExists(filterOptionsModel.OrderBy))
            {
                //then return bad request.
                return BadRequest();
            }

            //if fields are not valid.
            if (!VSafeUtils.TypeHasProperties<EmployeeDto>(filterOptionsModel.Fields))
            {
                //then return bad request.
                return BadRequest();
            }

            //get the paged/filtered show from db. 
            var employeesFromRepo = await _employeeService.GetFilteredEntities(filterOptionsModel);

            //if HATEOAS links are required.
            if (mediaType == "application/vnd.tourmanagement.employees.hateoas+json")
            {
                //create HATEOAS links for each show.
                employeesFromRepo.ForEach(employee =>
                {
                    var entityLinks = CreateLinksForEmployee(employee.Id, filterOptionsModel.Fields);
                    employee.links = entityLinks;
                });

                //prepare pagination metadata.
                var paginationMetadata = new
                {
                    totalCount = employeesFromRepo.TotalCount,
                    pageSize = employeesFromRepo.PageSize,
                    currentPage = employeesFromRepo.CurrentPage,
                    totalPages = employeesFromRepo.TotalPages,
                };

                //add pagination meta data to response header.
                Response.Headers.Add("X-Pagination", JsonConvert.SerializeObject(paginationMetadata));

                //create links for shows.
                var links = CreateLinksForEmployees(filterOptionsModel, employeesFromRepo.HasNext, employeesFromRepo.HasPrevious);

                //prepare model with data and HATEOAS links.
                var linkedCollectionResource = new
                {
                    value = employeesFromRepo,
                    links = links
                };

                //return the data with Ok response.
                return Ok(linkedCollectionResource);
            }
            else
            {
                var previousPageLink = employeesFromRepo.HasPrevious ?
                    CreateEmployeesResourceUri(filterOptionsModel, ResourceUriType.PreviousPage) : null;

                var nextPageLink = employeesFromRepo.HasNext ?
                    CreateEmployeesResourceUri(filterOptionsModel, ResourceUriType.NextPage) : null;

                //prepare the pagination metadata.
                var paginationMetadata = new
                {
                    previousPageLink = previousPageLink,
                    nextPageLink = nextPageLink,
                    totalCount = employeesFromRepo.TotalCount,
                    pageSize = employeesFromRepo.PageSize,
                    currentPage = employeesFromRepo.CurrentPage,
                    totalPages = employeesFromRepo.TotalPages
                };

                //add pagination meta data to response header.
                Response.Headers.Add("X-Pagination", JsonConvert.SerializeObject(paginationMetadata));

                //return the data with Ok response.
                return Ok(employeesFromRepo);
            }
        }


        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [Produces("application/vnd.tourmanagement.employees.hateoas+json", "application/json")]
        [HttpGet("{id}", Name = "GetEmployee")]
        public async Task<ActionResult<Employee>> GetEmployee(Guid id, [FromQuery] string fields, [FromHeader(Name = "Accept")] string mediaType)
        {
            object employeeEntity = null;
            object linkedResourceToReturn = null;

            //if fields are not passed.
            if (string.IsNullOrEmpty(fields))
            {
                _logger.LogInformation("GetEmployee called");

                //then get the whole entity and map it to the Dto.
                employeeEntity = Mapper.Map<EmployeeDto>(await _employeeService.GetEntityByIdAsync(id));
            }
            else
            {
                //if fields are passed then get the partial show.
                employeeEntity = await _employeeService.GetPartialEntityAsync(id, fields);
            }

            //if employee not found.
            if (employeeEntity == null)
            {
                //then return not found response.
                return NotFound();
            }

            if (mediaType == "application/vnd.tourmanagement.employees.hateoas+json")
            {
                //create HATEOS links
                var links = CreateLinksForEmployee(id, fields);

                //if fields are not passed.
                if (string.IsNullOrEmpty(fields))
                {
                    //convert the typed object to expando object.
                    linkedResourceToReturn = ((EmployeeDto)employeeEntity).ShapeData("") as IDictionary<string, object>;

                    //add the HATEOAS links to the model.
                    ((IDictionary<string, object>)linkedResourceToReturn).Add("links", links);
                }
                else
                {
                    linkedResourceToReturn = employeeEntity;

                    //add the HATEOAS links to the model.
                    ((dynamic)linkedResourceToReturn).links = links;

                }
            }
            else
            {
                linkedResourceToReturn = employeeEntity;
            }

            //return the Ok response.
            return Ok(linkedResourceToReturn);
        }

        #endregion


        #region HTTPPUT

        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpPut("{id}", Name = "UpdateEmployee")]
        public async Task<IActionResult> UpdateEmployee(Guid id, [FromBody]EmployeeForUpdate EmployeeForUpdate)
        {

            //if show not found
            if (!await _employeeService.ExistAsync(x => x.Id == id))
            {
                //then return not found response.
                return NotFound();
            }

            //Update an entity.
            await _employeeService.UpdateEntityAsync(id, EmployeeForUpdate);

            //return the response.
            return NoContent();
        }

        #endregion


        #region HTTPPATCH

        [Consumes("application/json-patch+json")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpPatch("{id}")]
        public async Task<IActionResult> PartiallyUpdateEmployee(Guid id, [FromBody] JsonPatchDocument<EmployeeForUpdate> jsonPatchDocument)
        {
            EmployeeForUpdate dto = new EmployeeForUpdate();
            Employee employee = new Employee();

            //apply the patch changes to the dto. 
            jsonPatchDocument.ApplyTo(dto, ModelState);

            //if the jsonPatchDocument is not valid.
            if (!ModelState.IsValid)
            {
                //then return unprocessableEntity response.
                return new UnprocessableEntityObjectResult(ModelState);
            }

            //if the dto model is not valid after applying changes.
            if (!TryValidateModel(dto))
            {
                //then return unprocessableEntity response.
                return new UnprocessableEntityObjectResult(ModelState);
            }

            //map the chnages from dto to entity.
            Mapper.Map(dto, employee);

            //set the Id for the show model.
            employee.Id = id;

            //partially update the chnages to the db. 
            await _employeeService.UpdatePartialEntityAsync(employee, jsonPatchDocument);

            //return the response.
            return NoContent();
        }

        #endregion


        #region HTTPPOST

        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpPost(Name = "CreateEmployee")]
        public async Task<ActionResult<EmployeeDto>> CreateEmployee([FromBody]EmployeeForCreation employee)
        {
            //create a show in db.
            var employeeToReturn = await _employeeService.CreateEntityAsync<EmployeeDto, EmployeeForCreation>(employee);

            //return the show created response.
            return CreatedAtRoute("GetEmployee", new { id = employeeToReturn.Id }, employeeToReturn);
        }

        #endregion


        #region HTTPDELETE
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpDelete("{id}", Name = "DeleteEmployeeById")]
        public async Task<IActionResult> DeleteEmployeeById(Guid id)
        {
            //if the employee exists
            if (await _employeeService.ExistAsync(x => x.Id == id))
            {
                //delete the employee from the db.
                await _employeeService.DeleteEntityAsync(id);
            }
            else
            {
                //if employee doesn't exists then returns not found.
                return NotFound();
            }

            //return the response.
            return NoContent();
        }

        #endregion


        #region PRIVATE METHODS

        private IEnumerable<LinkDto> CreateLinksForEmployee(Guid id, string fields)
        {
            var links = new List<LinkDto>();

            if (string.IsNullOrWhiteSpace(fields))
            {
                links.Add(
                  new LinkDto(_urlHelper.Link("GetEmployee", new { id = id }),
                  "self",
                  "GET"));
            }
            else
            {
                links.Add(
                  new LinkDto(_urlHelper.Link("GetEmployee", new { id = id, fields = fields }),
                  "self",
                  "GET"));
            }

            links.Add(
              new LinkDto(_urlHelper.Link("DeleteEmployeeById", new { id = id }),
              "delete_employee",
              "DELETE"));

            links.Add(
             new LinkDto(_urlHelper.Link("UpdateEmployee", new { id = id }),
             "update_employee",
             "PUT"));

            links.Add(
              new LinkDto(_urlHelper.Link("CreateEmployee", new { }),
              "create_employee",
              "POST"));

            //links.Add(
            //   new LinkDto(_urlHelper.Link("GetShowsForTour", new { }),
            //   "shows",
            //   "GET"));

            return links;
        }

        private IEnumerable<LinkDto> CreateLinksForEmployees(FilterOptionsModel filterOptionsModel, bool hasNext, bool hasPrevious)
        {
            var links = new List<LinkDto>();

            links.Add(
               new LinkDto(CreateEmployeesResourceUri(filterOptionsModel, ResourceUriType.Current), "self", "GET"));

            if (hasNext)
            {
                links.Add(
                  new LinkDto(CreateEmployeesResourceUri(filterOptionsModel, ResourceUriType.NextPage), "nextPage", "GET"));
            }

            if (hasPrevious)
            {
                links.Add(new LinkDto(CreateEmployeesResourceUri(filterOptionsModel, ResourceUriType.PreviousPage), "previousPage", "GET"));
            }

            return links;
        }

        private string CreateEmployeesResourceUri(FilterOptionsModel filterOptionsModel, ResourceUriType type)
        {
            switch (type)
            {
                case ResourceUriType.PreviousPage:
                    return _urlHelper.Link("GetFilteredEmployees",
                      new
                      {
                          fields = filterOptionsModel.Fields,
                          orderBy = filterOptionsModel.OrderBy,
                          searchQuery = filterOptionsModel.SearchQuery,
                          pageNumber = filterOptionsModel.PageNumber - 1,
                          pageSize = filterOptionsModel.PageSize
                      });
                case ResourceUriType.NextPage:
                    return _urlHelper.Link("GetFilteredEmployees",
                      new
                      {
                          fields = filterOptionsModel.Fields,
                          orderBy = filterOptionsModel.OrderBy,
                          searchQuery = filterOptionsModel.SearchQuery,
                          pageNumber = filterOptionsModel.PageNumber + 1,
                          pageSize = filterOptionsModel.PageSize
                      });

                default:
                    return _urlHelper.Link("GetFilteredEmployees",
                    new
                    {
                        fields = filterOptionsModel.Fields,
                        orderBy = filterOptionsModel.OrderBy,
                        searchQuery = filterOptionsModel.SearchQuery,
                        pageNumber = filterOptionsModel.PageNumber,
                        pageSize = filterOptionsModel.PageSize
                    });
            }
        }

        #endregion

    }
}
