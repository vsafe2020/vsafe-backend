using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
//using VSafe.API.Helpers;
using VSafe.Dto;
using VSafe.Service;
using VSafe.Domain.Entities;
using VSafe.Utility;
using Microsoft.Extensions.Logging;

namespace VSafe.API.Controllers
{
    /// <summary>
    /// ExternalContact endpoint
    /// </summary>
    [Route("api/externalcontacts")]
    [Produces("application/json")]
    [ApiController]
    public class ExternalContactController : Controller
    {

        #region PRIVATE MEMBERS

        private readonly IExternalContactService _externalcontactService;
        private ILogger<ExternalContactController> _logger;
        private readonly IUrlHelper _urlHelper;

        #endregion


        #region CONSTRUCTOR

        public ExternalContactController(IExternalContactService externalcontactService, ILogger<ExternalContactController> logger, IUrlHelper urlHelper) 
        {
            _logger = logger;
            _externalcontactService = externalcontactService;
            _urlHelper = urlHelper;
        }

        #endregion


        #region HTTPGET

        [HttpGet(Name = "GetFilteredExternalContacts")]
        [Produces("application/vnd.tourmanagement.externalcontacts.hateoas+json", "application/json")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<List<ExternalContactDto>>> GetFilteredExternalContacts([FromQuery]FilterOptionsModel filterOptionsModel, [FromHeader(Name = "Accept")] string mediaType)
        {

            //if order by fields are not valid.
            if (!_externalcontactService.ValidMappingExists(filterOptionsModel.OrderBy))
            {
                //then return bad request.
                return BadRequest();
            }

            //if fields are not valid.
            if (!VSafeUtils.TypeHasProperties<ExternalContactDto>(filterOptionsModel.Fields))
            {
                //then return bad request.
                return BadRequest();
            }

            //get the paged/filtered show from db. 
            var externalcontactsFromRepo = await _externalcontactService.GetFilteredEntities(filterOptionsModel);

            //if HATEOAS links are required.
            if (mediaType == "application/vnd.tourmanagement.externalcontacts.hateoas+json")
            {
                //create HATEOAS links for each show.
                externalcontactsFromRepo.ForEach(externalcontact =>
                {
                    var entityLinks = CreateLinksForExternalContact(externalcontact.Id, filterOptionsModel.Fields);
                    externalcontact.links = entityLinks;
                });

                //prepare pagination metadata.
                var paginationMetadata = new
                {
                    totalCount = externalcontactsFromRepo.TotalCount,
                    pageSize = externalcontactsFromRepo.PageSize,
                    currentPage = externalcontactsFromRepo.CurrentPage,
                    totalPages = externalcontactsFromRepo.TotalPages,
                };

                //add pagination meta data to response header.
                Response.Headers.Add("X-Pagination", JsonConvert.SerializeObject(paginationMetadata));

                //create links for shows.
                var links = CreateLinksForExternalContacts(filterOptionsModel, externalcontactsFromRepo.HasNext, externalcontactsFromRepo.HasPrevious);

                //prepare model with data and HATEOAS links.
                var linkedCollectionResource = new
                {
                    value = externalcontactsFromRepo,
                    links = links
                };

                //return the data with Ok response.
                return Ok(linkedCollectionResource);
            }
            else
            {
                var previousPageLink = externalcontactsFromRepo.HasPrevious ?
                    CreateExternalContactsResourceUri(filterOptionsModel, ResourceUriType.PreviousPage) : null;

                var nextPageLink = externalcontactsFromRepo.HasNext ?
                    CreateExternalContactsResourceUri(filterOptionsModel, ResourceUriType.NextPage) : null;

                //prepare the pagination metadata.
                var paginationMetadata = new
                {
                    previousPageLink = previousPageLink,
                    nextPageLink = nextPageLink,
                    totalCount = externalcontactsFromRepo.TotalCount,
                    pageSize = externalcontactsFromRepo.PageSize,
                    currentPage = externalcontactsFromRepo.CurrentPage,
                    totalPages = externalcontactsFromRepo.TotalPages
                };

                //add pagination meta data to response header.
                Response.Headers.Add("X-Pagination", JsonConvert.SerializeObject(paginationMetadata));

                //return the data with Ok response.
                return Ok(externalcontactsFromRepo);
            }
        }


        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [Produces("application/vnd.tourmanagement.externalcontacts.hateoas+json", "application/json")]
        [HttpGet("{id}", Name = "GetExternalContact")]
        public async Task<ActionResult<ExternalContact>> GetExternalContact(Guid id, [FromQuery] string fields, [FromHeader(Name = "Accept")] string mediaType)
        {
            object externalcontactEntity = null;
            object linkedResourceToReturn = null;

            //if fields are not passed.
            if (string.IsNullOrEmpty(fields))
            {
                _logger.LogInformation("GetExternalContact called");

                //then get the whole entity and map it to the Dto.
                externalcontactEntity = Mapper.Map<ExternalContactDto>(await _externalcontactService.GetEntityByIdAsync(id));
            }
            else
            {
                //if fields are passed then get the partial show.
                externalcontactEntity = await _externalcontactService.GetPartialEntityAsync(id, fields);
            }

            //if externalcontact not found.
            if (externalcontactEntity == null)
            {
                //then return not found response.
                return NotFound();
            }

            if (mediaType == "application/vnd.tourmanagement.externalcontacts.hateoas+json")
            {
                //create HATEOS links
                var links = CreateLinksForExternalContact(id, fields);

                //if fields are not passed.
                if (string.IsNullOrEmpty(fields))
                {
                    //convert the typed object to expando object.
                    linkedResourceToReturn = ((ExternalContactDto)externalcontactEntity).ShapeData("") as IDictionary<string, object>;

                    //add the HATEOAS links to the model.
                    ((IDictionary<string, object>)linkedResourceToReturn).Add("links", links);
                }
                else
                {
                    linkedResourceToReturn = externalcontactEntity;

                    //add the HATEOAS links to the model.
                    ((dynamic)linkedResourceToReturn).links = links;

                }
            }
            else
            {
                linkedResourceToReturn = externalcontactEntity;
            }

            //return the Ok response.
            return Ok(linkedResourceToReturn);
        }

        #endregion


        #region HTTPPUT

        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpPut("{id}", Name = "UpdateExternalContact")]
        public async Task<IActionResult> UpdateExternalContact(Guid id, [FromBody]ExternalContactForUpdate ExternalContactForUpdate)
        {

            //if show not found
            if (!await _externalcontactService.ExistAsync(x => x.Id == id))
            {
                //then return not found response.
                return NotFound();
            }

            //Update an entity.
            await _externalcontactService.UpdateEntityAsync(id, ExternalContactForUpdate);

            //return the response.
            return NoContent();
        }

        #endregion


        #region HTTPPATCH

        [Consumes("application/json-patch+json")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpPatch("{id}")]
        public async Task<IActionResult> PartiallyUpdateExternalContact(Guid id, [FromBody] JsonPatchDocument<ExternalContactForUpdate> jsonPatchDocument)
        {
            ExternalContactForUpdate dto = new ExternalContactForUpdate();
            ExternalContact externalcontact = new ExternalContact();

            //apply the patch changes to the dto. 
            jsonPatchDocument.ApplyTo(dto, ModelState);

            //if the jsonPatchDocument is not valid.
            if (!ModelState.IsValid)
            {
                //then return unprocessableEntity response.
                return new UnprocessableEntityObjectResult(ModelState);
            }

            //if the dto model is not valid after applying changes.
            if (!TryValidateModel(dto))
            {
                //then return unprocessableEntity response.
                return new UnprocessableEntityObjectResult(ModelState);
            }

            //map the chnages from dto to entity.
            Mapper.Map(dto, externalcontact);

            //set the Id for the show model.
            externalcontact.Id = id;

            //partially update the chnages to the db. 
            await _externalcontactService.UpdatePartialEntityAsync(externalcontact, jsonPatchDocument);

            //return the response.
            return NoContent();
        }

        #endregion


        #region HTTPPOST

        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpPost(Name = "CreateExternalContact")]
        public async Task<ActionResult<ExternalContactDto>> CreateExternalContact([FromBody]ExternalContactForCreation externalcontact)
        {
            //create a show in db.
            var externalcontactToReturn = await _externalcontactService.CreateEntityAsync<ExternalContactDto, ExternalContactForCreation>(externalcontact);

            //return the show created response.
            return CreatedAtRoute("GetExternalContact", new { id = externalcontactToReturn.Id }, externalcontactToReturn);
        }

        #endregion


        #region HTTPDELETE
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpDelete("{id}", Name = "DeleteExternalContactById")]
        public async Task<IActionResult> DeleteExternalContactById(Guid id)
        {
            //if the externalcontact exists
            if (await _externalcontactService.ExistAsync(x => x.Id == id))
            {
                //delete the externalcontact from the db.
                await _externalcontactService.DeleteEntityAsync(id);
            }
            else
            {
                //if externalcontact doesn't exists then returns not found.
                return NotFound();
            }

            //return the response.
            return NoContent();
        }

        #endregion


        #region PRIVATE METHODS

        private IEnumerable<LinkDto> CreateLinksForExternalContact(Guid id, string fields)
        {
            var links = new List<LinkDto>();

            if (string.IsNullOrWhiteSpace(fields))
            {
                links.Add(
                  new LinkDto(_urlHelper.Link("GetExternalContact", new { id = id }),
                  "self",
                  "GET"));
            }
            else
            {
                links.Add(
                  new LinkDto(_urlHelper.Link("GetExternalContact", new { id = id, fields = fields }),
                  "self",
                  "GET"));
            }

            links.Add(
              new LinkDto(_urlHelper.Link("DeleteExternalContactById", new { id = id }),
              "delete_externalcontact",
              "DELETE"));

            links.Add(
             new LinkDto(_urlHelper.Link("UpdateExternalContact", new { id = id }),
             "update_externalcontact",
             "PUT"));

            links.Add(
              new LinkDto(_urlHelper.Link("CreateExternalContact", new { }),
              "create_externalcontact",
              "POST"));

            //links.Add(
            //   new LinkDto(_urlHelper.Link("GetShowsForTour", new { }),
            //   "shows",
            //   "GET"));

            return links;
        }

        private IEnumerable<LinkDto> CreateLinksForExternalContacts(FilterOptionsModel filterOptionsModel, bool hasNext, bool hasPrevious)
        {
            var links = new List<LinkDto>();

            links.Add(
               new LinkDto(CreateExternalContactsResourceUri(filterOptionsModel, ResourceUriType.Current), "self", "GET"));

            if (hasNext)
            {
                links.Add(
                  new LinkDto(CreateExternalContactsResourceUri(filterOptionsModel, ResourceUriType.NextPage), "nextPage", "GET"));
            }

            if (hasPrevious)
            {
                links.Add(new LinkDto(CreateExternalContactsResourceUri(filterOptionsModel, ResourceUriType.PreviousPage), "previousPage", "GET"));
            }

            return links;
        }

        private string CreateExternalContactsResourceUri(FilterOptionsModel filterOptionsModel, ResourceUriType type)
        {
            switch (type)
            {
                case ResourceUriType.PreviousPage:
                    return _urlHelper.Link("GetFilteredExternalContacts",
                      new
                      {
                          fields = filterOptionsModel.Fields,
                          orderBy = filterOptionsModel.OrderBy,
                          searchQuery = filterOptionsModel.SearchQuery,
                          pageNumber = filterOptionsModel.PageNumber - 1,
                          pageSize = filterOptionsModel.PageSize
                      });
                case ResourceUriType.NextPage:
                    return _urlHelper.Link("GetFilteredExternalContacts",
                      new
                      {
                          fields = filterOptionsModel.Fields,
                          orderBy = filterOptionsModel.OrderBy,
                          searchQuery = filterOptionsModel.SearchQuery,
                          pageNumber = filterOptionsModel.PageNumber + 1,
                          pageSize = filterOptionsModel.PageSize
                      });

                default:
                    return _urlHelper.Link("GetFilteredExternalContacts",
                    new
                    {
                        fields = filterOptionsModel.Fields,
                        orderBy = filterOptionsModel.OrderBy,
                        searchQuery = filterOptionsModel.SearchQuery,
                        pageNumber = filterOptionsModel.PageNumber,
                        pageSize = filterOptionsModel.PageSize
                    });
            }
        }

        #endregion

    }
}
