using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
//using VSafe.API.Helpers;
using VSafe.Dto;
using VSafe.Service;
using VSafe.Domain.Entities;
using VSafe.Utility;
using Microsoft.Extensions.Logging;

namespace VSafe.API.Controllers
{
    /// <summary>
    /// ExternalExposers endpoint
    /// </summary>
    [Route("api/externalexposerss")]
    [Produces("application/json")]
    [ApiController]
    public class ExternalExposersController : Controller
    {

        #region PRIVATE MEMBERS

        private readonly IExternalExposersService _externalexposersService;
        private ILogger<ExternalExposersController> _logger;
        private readonly IUrlHelper _urlHelper;

        #endregion


        #region CONSTRUCTOR

        public ExternalExposersController(IExternalExposersService externalexposersService, ILogger<ExternalExposersController> logger, IUrlHelper urlHelper) 
        {
            _logger = logger;
            _externalexposersService = externalexposersService;
            _urlHelper = urlHelper;
        }

        #endregion


        #region HTTPGET

        [HttpGet(Name = "GetFilteredExternalExposerss")]
        [Produces("application/vnd.tourmanagement.externalexposerss.hateoas+json", "application/json")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<List<ExternalExposersDto>>> GetFilteredExternalExposerss([FromQuery]FilterOptionsModel filterOptionsModel, [FromHeader(Name = "Accept")] string mediaType)
        {

            //if order by fields are not valid.
            if (!_externalexposersService.ValidMappingExists(filterOptionsModel.OrderBy))
            {
                //then return bad request.
                return BadRequest();
            }

            //if fields are not valid.
            if (!VSafeUtils.TypeHasProperties<ExternalExposersDto>(filterOptionsModel.Fields))
            {
                //then return bad request.
                return BadRequest();
            }

            //get the paged/filtered show from db. 
            var externalexposerssFromRepo = await _externalexposersService.GetFilteredEntities(filterOptionsModel);

            //if HATEOAS links are required.
            if (mediaType == "application/vnd.tourmanagement.externalexposerss.hateoas+json")
            {
                //create HATEOAS links for each show.
                externalexposerssFromRepo.ForEach(externalexposers =>
                {
                    var entityLinks = CreateLinksForExternalExposers(externalexposers.Id, filterOptionsModel.Fields);
                    externalexposers.links = entityLinks;
                });

                //prepare pagination metadata.
                var paginationMetadata = new
                {
                    totalCount = externalexposerssFromRepo.TotalCount,
                    pageSize = externalexposerssFromRepo.PageSize,
                    currentPage = externalexposerssFromRepo.CurrentPage,
                    totalPages = externalexposerssFromRepo.TotalPages,
                };

                //add pagination meta data to response header.
                Response.Headers.Add("X-Pagination", JsonConvert.SerializeObject(paginationMetadata));

                //create links for shows.
                var links = CreateLinksForExternalExposerss(filterOptionsModel, externalexposerssFromRepo.HasNext, externalexposerssFromRepo.HasPrevious);

                //prepare model with data and HATEOAS links.
                var linkedCollectionResource = new
                {
                    value = externalexposerssFromRepo,
                    links = links
                };

                //return the data with Ok response.
                return Ok(linkedCollectionResource);
            }
            else
            {
                var previousPageLink = externalexposerssFromRepo.HasPrevious ?
                    CreateExternalExposerssResourceUri(filterOptionsModel, ResourceUriType.PreviousPage) : null;

                var nextPageLink = externalexposerssFromRepo.HasNext ?
                    CreateExternalExposerssResourceUri(filterOptionsModel, ResourceUriType.NextPage) : null;

                //prepare the pagination metadata.
                var paginationMetadata = new
                {
                    previousPageLink = previousPageLink,
                    nextPageLink = nextPageLink,
                    totalCount = externalexposerssFromRepo.TotalCount,
                    pageSize = externalexposerssFromRepo.PageSize,
                    currentPage = externalexposerssFromRepo.CurrentPage,
                    totalPages = externalexposerssFromRepo.TotalPages
                };

                //add pagination meta data to response header.
                Response.Headers.Add("X-Pagination", JsonConvert.SerializeObject(paginationMetadata));

                //return the data with Ok response.
                return Ok(externalexposerssFromRepo);
            }
        }


        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [Produces("application/vnd.tourmanagement.externalexposerss.hateoas+json", "application/json")]
        [HttpGet("{id}", Name = "GetExternalExposers")]
        public async Task<ActionResult<ExternalExposer>> GetExternalExposers(Guid id, [FromQuery] string fields, [FromHeader(Name = "Accept")] string mediaType)
        {
            object externalexposersEntity = null;
            object linkedResourceToReturn = null;

            //if fields are not passed.
            if (string.IsNullOrEmpty(fields))
            {
                _logger.LogInformation("GetExternalExposers called");

                //then get the whole entity and map it to the Dto.
                externalexposersEntity = Mapper.Map<ExternalExposersDto>(await _externalexposersService.GetEntityByIdAsync(id));
            }
            else
            {
                //if fields are passed then get the partial show.
                externalexposersEntity = await _externalexposersService.GetPartialEntityAsync(id, fields);
            }

            //if externalexposers not found.
            if (externalexposersEntity == null)
            {
                //then return not found response.
                return NotFound();
            }

            if (mediaType == "application/vnd.tourmanagement.externalexposerss.hateoas+json")
            {
                //create HATEOS links
                var links = CreateLinksForExternalExposers(id, fields);

                //if fields are not passed.
                if (string.IsNullOrEmpty(fields))
                {
                    //convert the typed object to expando object.
                    linkedResourceToReturn = ((ExternalExposersDto)externalexposersEntity).ShapeData("") as IDictionary<string, object>;

                    //add the HATEOAS links to the model.
                    ((IDictionary<string, object>)linkedResourceToReturn).Add("links", links);
                }
                else
                {
                    linkedResourceToReturn = externalexposersEntity;

                    //add the HATEOAS links to the model.
                    ((dynamic)linkedResourceToReturn).links = links;

                }
            }
            else
            {
                linkedResourceToReturn = externalexposersEntity;
            }

            //return the Ok response.
            return Ok(linkedResourceToReturn);
        }

        #endregion


        #region HTTPPUT

        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpPut("{id}", Name = "UpdateExternalExposers")]
        public async Task<IActionResult> UpdateExternalExposers(Guid id, [FromBody]ExternalExposersForUpdate ExternalExposersForUpdate)
        {

            //if show not found
            if (!await _externalexposersService.ExistAsync(x => x.Id == id))
            {
                //then return not found response.
                return NotFound();
            }

            //Update an entity.
            await _externalexposersService.UpdateEntityAsync(id, ExternalExposersForUpdate);

            //return the response.
            return NoContent();
        }

        #endregion


        #region HTTPPATCH

        [Consumes("application/json-patch+json")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpPatch("{id}")]
        public async Task<IActionResult> PartiallyUpdateExternalExposers(Guid id, [FromBody] JsonPatchDocument<ExternalExposersForUpdate> jsonPatchDocument)
        {
            ExternalExposersForUpdate dto = new ExternalExposersForUpdate();
            ExternalExposer externalexposers = new ExternalExposer();

            //apply the patch changes to the dto. 
            jsonPatchDocument.ApplyTo(dto, ModelState);

            //if the jsonPatchDocument is not valid.
            if (!ModelState.IsValid)
            {
                //then return unprocessableEntity response.
                return new UnprocessableEntityObjectResult(ModelState);
            }

            //if the dto model is not valid after applying changes.
            if (!TryValidateModel(dto))
            {
                //then return unprocessableEntity response.
                return new UnprocessableEntityObjectResult(ModelState);
            }

            //map the chnages from dto to entity.
            Mapper.Map(dto, externalexposers);

            //set the Id for the show model.
            externalexposers.Id = id;

            //partially update the chnages to the db. 
            await _externalexposersService.UpdatePartialEntityAsync(externalexposers, jsonPatchDocument);

            //return the response.
            return NoContent();
        }

        #endregion


        #region HTTPPOST

        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpPost(Name = "CreateExternalExposers")]
        public async Task<ActionResult<ExternalExposersDto>> CreateExternalExposers([FromBody]ExternalExposersForCreation externalexposers)
        {
            //create a show in db.
            var externalexposersToReturn = await _externalexposersService.CreateEntityAsync<ExternalExposersDto, ExternalExposersForCreation>(externalexposers);

            //return the show created response.
            return CreatedAtRoute("GetExternalExposers", new { id = externalexposersToReturn.Id }, externalexposersToReturn);
        }

        #endregion


        #region HTTPDELETE
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpDelete("{id}", Name = "DeleteExternalExposersById")]
        public async Task<IActionResult> DeleteExternalExposersById(Guid id)
        {
            //if the externalexposers exists
            if (await _externalexposersService.ExistAsync(x => x.Id == id))
            {
                //delete the externalexposers from the db.
                await _externalexposersService.DeleteEntityAsync(id);
            }
            else
            {
                //if externalexposers doesn't exists then returns not found.
                return NotFound();
            }

            //return the response.
            return NoContent();
        }

        #endregion


        #region PRIVATE METHODS

        private IEnumerable<LinkDto> CreateLinksForExternalExposers(Guid id, string fields)
        {
            var links = new List<LinkDto>();

            if (string.IsNullOrWhiteSpace(fields))
            {
                links.Add(
                  new LinkDto(_urlHelper.Link("GetExternalExposers", new { id = id }),
                  "self",
                  "GET"));
            }
            else
            {
                links.Add(
                  new LinkDto(_urlHelper.Link("GetExternalExposers", new { id = id, fields = fields }),
                  "self",
                  "GET"));
            }

            links.Add(
              new LinkDto(_urlHelper.Link("DeleteExternalExposersById", new { id = id }),
              "delete_externalexposers",
              "DELETE"));

            links.Add(
             new LinkDto(_urlHelper.Link("UpdateExternalExposers", new { id = id }),
             "update_externalexposers",
             "PUT"));

            links.Add(
              new LinkDto(_urlHelper.Link("CreateExternalExposers", new { }),
              "create_externalexposers",
              "POST"));

            //links.Add(
            //   new LinkDto(_urlHelper.Link("GetShowsForTour", new { }),
            //   "shows",
            //   "GET"));

            return links;
        }

        private IEnumerable<LinkDto> CreateLinksForExternalExposerss(FilterOptionsModel filterOptionsModel, bool hasNext, bool hasPrevious)
        {
            var links = new List<LinkDto>();

            links.Add(
               new LinkDto(CreateExternalExposerssResourceUri(filterOptionsModel, ResourceUriType.Current), "self", "GET"));

            if (hasNext)
            {
                links.Add(
                  new LinkDto(CreateExternalExposerssResourceUri(filterOptionsModel, ResourceUriType.NextPage), "nextPage", "GET"));
            }

            if (hasPrevious)
            {
                links.Add(new LinkDto(CreateExternalExposerssResourceUri(filterOptionsModel, ResourceUriType.PreviousPage), "previousPage", "GET"));
            }

            return links;
        }

        private string CreateExternalExposerssResourceUri(FilterOptionsModel filterOptionsModel, ResourceUriType type)
        {
            switch (type)
            {
                case ResourceUriType.PreviousPage:
                    return _urlHelper.Link("GetFilteredExternalExposerss",
                      new
                      {
                          fields = filterOptionsModel.Fields,
                          orderBy = filterOptionsModel.OrderBy,
                          searchQuery = filterOptionsModel.SearchQuery,
                          pageNumber = filterOptionsModel.PageNumber - 1,
                          pageSize = filterOptionsModel.PageSize
                      });
                case ResourceUriType.NextPage:
                    return _urlHelper.Link("GetFilteredExternalExposerss",
                      new
                      {
                          fields = filterOptionsModel.Fields,
                          orderBy = filterOptionsModel.OrderBy,
                          searchQuery = filterOptionsModel.SearchQuery,
                          pageNumber = filterOptionsModel.PageNumber + 1,
                          pageSize = filterOptionsModel.PageSize
                      });

                default:
                    return _urlHelper.Link("GetFilteredExternalExposerss",
                    new
                    {
                        fields = filterOptionsModel.Fields,
                        orderBy = filterOptionsModel.OrderBy,
                        searchQuery = filterOptionsModel.SearchQuery,
                        pageNumber = filterOptionsModel.PageNumber,
                        pageSize = filterOptionsModel.PageSize
                    });
            }
        }

        #endregion

    }
}
