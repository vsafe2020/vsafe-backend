using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
//using VSafe.API.Helpers;
using VSafe.Dto;
using VSafe.Service;
using VSafe.Domain.Entities;
using VSafe.Utility;
using Microsoft.Extensions.Logging;

namespace VSafe.API.Controllers
{
    /// <summary>
    /// EmployeeExposers endpoint
    /// </summary>
    [Route("api/employeeexposerss")]
    [Produces("application/json")]
    [ApiController]
    public class EmployeeExposersController : Controller
    {

        #region PRIVATE MEMBERS

        private readonly IEmployeeExposersService _employeeexposersService;
        private ILogger<EmployeeExposersController> _logger;
        private readonly IUrlHelper _urlHelper;

        #endregion


        #region CONSTRUCTOR

        public EmployeeExposersController(IEmployeeExposersService employeeexposersService, ILogger<EmployeeExposersController> logger, IUrlHelper urlHelper) 
        {
            _logger = logger;
            _employeeexposersService = employeeexposersService;
            _urlHelper = urlHelper;
        }

        #endregion


        #region HTTPGET

        [HttpGet(Name = "GetFilteredEmployeeExposerss")]
        [Produces("application/vnd.tourmanagement.employeeexposerss.hateoas+json", "application/json")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<List<EmployeeExposersDto>>> GetFilteredEmployeeExposerss([FromQuery]FilterOptionsModel filterOptionsModel, [FromHeader(Name = "Accept")] string mediaType)
        {

            //if order by fields are not valid.
            if (!_employeeexposersService.ValidMappingExists(filterOptionsModel.OrderBy))
            {
                //then return bad request.
                return BadRequest();
            }

            //if fields are not valid.
            if (!VSafeUtils.TypeHasProperties<EmployeeExposersDto>(filterOptionsModel.Fields))
            {
                //then return bad request.
                return BadRequest();
            }

            //get the paged/filtered show from db. 
            var employeeexposerssFromRepo = await _employeeexposersService.GetFilteredEntities(filterOptionsModel);

            //if HATEOAS links are required.
            if (mediaType == "application/vnd.tourmanagement.employeeexposerss.hateoas+json")
            {
                //create HATEOAS links for each show.
                employeeexposerssFromRepo.ForEach(employeeexposers =>
                {
                    var entityLinks = CreateLinksForEmployeeExposers(employeeexposers.Id, filterOptionsModel.Fields);
                    employeeexposers.links = entityLinks;
                });

                //prepare pagination metadata.
                var paginationMetadata = new
                {
                    totalCount = employeeexposerssFromRepo.TotalCount,
                    pageSize = employeeexposerssFromRepo.PageSize,
                    currentPage = employeeexposerssFromRepo.CurrentPage,
                    totalPages = employeeexposerssFromRepo.TotalPages,
                };

                //add pagination meta data to response header.
                Response.Headers.Add("X-Pagination", JsonConvert.SerializeObject(paginationMetadata));

                //create links for shows.
                var links = CreateLinksForEmployeeExposerss(filterOptionsModel, employeeexposerssFromRepo.HasNext, employeeexposerssFromRepo.HasPrevious);

                //prepare model with data and HATEOAS links.
                var linkedCollectionResource = new
                {
                    value = employeeexposerssFromRepo,
                    links = links
                };

                //return the data with Ok response.
                return Ok(linkedCollectionResource);
            }
            else
            {
                var previousPageLink = employeeexposerssFromRepo.HasPrevious ?
                    CreateEmployeeExposerssResourceUri(filterOptionsModel, ResourceUriType.PreviousPage) : null;

                var nextPageLink = employeeexposerssFromRepo.HasNext ?
                    CreateEmployeeExposerssResourceUri(filterOptionsModel, ResourceUriType.NextPage) : null;

                //prepare the pagination metadata.
                var paginationMetadata = new
                {
                    previousPageLink = previousPageLink,
                    nextPageLink = nextPageLink,
                    totalCount = employeeexposerssFromRepo.TotalCount,
                    pageSize = employeeexposerssFromRepo.PageSize,
                    currentPage = employeeexposerssFromRepo.CurrentPage,
                    totalPages = employeeexposerssFromRepo.TotalPages
                };

                //add pagination meta data to response header.
                Response.Headers.Add("X-Pagination", JsonConvert.SerializeObject(paginationMetadata));

                //return the data with Ok response.
                return Ok(employeeexposerssFromRepo);
            }
        }


        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [Produces("application/vnd.tourmanagement.employeeexposerss.hateoas+json", "application/json")]
        [HttpGet("{id}", Name = "GetEmployeeExposers")]
        public async Task<ActionResult<EmployeeExposer>> GetEmployeeExposers(Guid id, [FromQuery] string fields, [FromHeader(Name = "Accept")] string mediaType)
        {
            object employeeexposersEntity = null;
            object linkedResourceToReturn = null;

            //if fields are not passed.
            if (string.IsNullOrEmpty(fields))
            {
                _logger.LogInformation("GetEmployeeExposers called");

                //then get the whole entity and map it to the Dto.
                employeeexposersEntity = Mapper.Map<EmployeeExposersDto>(await _employeeexposersService.GetEntityByIdAsync(id));
            }
            else
            {
                //if fields are passed then get the partial show.
                employeeexposersEntity = await _employeeexposersService.GetPartialEntityAsync(id, fields);
            }

            //if employeeexposers not found.
            if (employeeexposersEntity == null)
            {
                //then return not found response.
                return NotFound();
            }

            if (mediaType == "application/vnd.tourmanagement.employeeexposerss.hateoas+json")
            {
                //create HATEOS links
                var links = CreateLinksForEmployeeExposers(id, fields);

                //if fields are not passed.
                if (string.IsNullOrEmpty(fields))
                {
                    //convert the typed object to expando object.
                    linkedResourceToReturn = ((EmployeeExposersDto)employeeexposersEntity).ShapeData("") as IDictionary<string, object>;

                    //add the HATEOAS links to the model.
                    ((IDictionary<string, object>)linkedResourceToReturn).Add("links", links);
                }
                else
                {
                    linkedResourceToReturn = employeeexposersEntity;

                    //add the HATEOAS links to the model.
                    ((dynamic)linkedResourceToReturn).links = links;

                }
            }
            else
            {
                linkedResourceToReturn = employeeexposersEntity;
            }

            //return the Ok response.
            return Ok(linkedResourceToReturn);
        }

        #endregion


        #region HTTPPUT

        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpPut("{id}", Name = "UpdateEmployeeExposers")]
        public async Task<IActionResult> UpdateEmployeeExposers(Guid id, [FromBody]EmployeeExposersForUpdate EmployeeExposersForUpdate)
        {

            //if show not found
            if (!await _employeeexposersService.ExistAsync(x => x.Id == id))
            {
                //then return not found response.
                return NotFound();
            }

            //Update an entity.
            await _employeeexposersService.UpdateEntityAsync(id, EmployeeExposersForUpdate);

            //return the response.
            return NoContent();
        }

        #endregion


        #region HTTPPATCH

        [Consumes("application/json-patch+json")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpPatch("{id}")]
        public async Task<IActionResult> PartiallyUpdateEmployeeExposers(Guid id, [FromBody] JsonPatchDocument<EmployeeExposersForUpdate> jsonPatchDocument)
        {
            EmployeeExposersForUpdate dto = new EmployeeExposersForUpdate();
            EmployeeExposer employeeexposers = new EmployeeExposer();

            //apply the patch changes to the dto. 
            jsonPatchDocument.ApplyTo(dto, ModelState);

            //if the jsonPatchDocument is not valid.
            if (!ModelState.IsValid)
            {
                //then return unprocessableEntity response.
                return new UnprocessableEntityObjectResult(ModelState);
            }

            //if the dto model is not valid after applying changes.
            if (!TryValidateModel(dto))
            {
                //then return unprocessableEntity response.
                return new UnprocessableEntityObjectResult(ModelState);
            }

            //map the chnages from dto to entity.
            Mapper.Map(dto, employeeexposers);

            //set the Id for the show model.
            employeeexposers.Id = id;

            //partially update the chnages to the db. 
            await _employeeexposersService.UpdatePartialEntityAsync(employeeexposers, jsonPatchDocument);

            //return the response.
            return NoContent();
        }

        #endregion


        #region HTTPPOST

        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpPost(Name = "CreateEmployeeExposers")]
        public async Task<ActionResult<EmployeeExposersDto>> CreateEmployeeExposers([FromBody]EmployeeExposersForCreation employeeexposers)
        {
            //create a show in db.
            var employeeexposersToReturn = await _employeeexposersService.CreateEntityAsync<EmployeeExposersDto, EmployeeExposersForCreation>(employeeexposers);

            //return the show created response.
            return CreatedAtRoute("GetEmployeeExposers", new { id = employeeexposersToReturn.Id }, employeeexposersToReturn);
        }

        #endregion


        #region HTTPDELETE
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpDelete("{id}", Name = "DeleteEmployeeExposersById")]
        public async Task<IActionResult> DeleteEmployeeExposersById(Guid id)
        {
            //if the employeeexposers exists
            if (await _employeeexposersService.ExistAsync(x => x.Id == id))
            {
                //delete the employeeexposers from the db.
                await _employeeexposersService.DeleteEntityAsync(id);
            }
            else
            {
                //if employeeexposers doesn't exists then returns not found.
                return NotFound();
            }

            //return the response.
            return NoContent();
        }

        #endregion


        #region PRIVATE METHODS

        private IEnumerable<LinkDto> CreateLinksForEmployeeExposers(Guid id, string fields)
        {
            var links = new List<LinkDto>();

            if (string.IsNullOrWhiteSpace(fields))
            {
                links.Add(
                  new LinkDto(_urlHelper.Link("GetEmployeeExposers", new { id = id }),
                  "self",
                  "GET"));
            }
            else
            {
                links.Add(
                  new LinkDto(_urlHelper.Link("GetEmployeeExposers", new { id = id, fields = fields }),
                  "self",
                  "GET"));
            }

            links.Add(
              new LinkDto(_urlHelper.Link("DeleteEmployeeExposersById", new { id = id }),
              "delete_employeeexposers",
              "DELETE"));

            links.Add(
             new LinkDto(_urlHelper.Link("UpdateEmployeeExposers", new { id = id }),
             "update_employeeexposers",
             "PUT"));

            links.Add(
              new LinkDto(_urlHelper.Link("CreateEmployeeExposers", new { }),
              "create_employeeexposers",
              "POST"));

            //links.Add(
            //   new LinkDto(_urlHelper.Link("GetShowsForTour", new { }),
            //   "shows",
            //   "GET"));

            return links;
        }

        private IEnumerable<LinkDto> CreateLinksForEmployeeExposerss(FilterOptionsModel filterOptionsModel, bool hasNext, bool hasPrevious)
        {
            var links = new List<LinkDto>();

            links.Add(
               new LinkDto(CreateEmployeeExposerssResourceUri(filterOptionsModel, ResourceUriType.Current), "self", "GET"));

            if (hasNext)
            {
                links.Add(
                  new LinkDto(CreateEmployeeExposerssResourceUri(filterOptionsModel, ResourceUriType.NextPage), "nextPage", "GET"));
            }

            if (hasPrevious)
            {
                links.Add(new LinkDto(CreateEmployeeExposerssResourceUri(filterOptionsModel, ResourceUriType.PreviousPage), "previousPage", "GET"));
            }

            return links;
        }

        private string CreateEmployeeExposerssResourceUri(FilterOptionsModel filterOptionsModel, ResourceUriType type)
        {
            switch (type)
            {
                case ResourceUriType.PreviousPage:
                    return _urlHelper.Link("GetFilteredEmployeeExposerss",
                      new
                      {
                          fields = filterOptionsModel.Fields,
                          orderBy = filterOptionsModel.OrderBy,
                          searchQuery = filterOptionsModel.SearchQuery,
                          pageNumber = filterOptionsModel.PageNumber - 1,
                          pageSize = filterOptionsModel.PageSize
                      });
                case ResourceUriType.NextPage:
                    return _urlHelper.Link("GetFilteredEmployeeExposerss",
                      new
                      {
                          fields = filterOptionsModel.Fields,
                          orderBy = filterOptionsModel.OrderBy,
                          searchQuery = filterOptionsModel.SearchQuery,
                          pageNumber = filterOptionsModel.PageNumber + 1,
                          pageSize = filterOptionsModel.PageSize
                      });

                default:
                    return _urlHelper.Link("GetFilteredEmployeeExposerss",
                    new
                    {
                        fields = filterOptionsModel.Fields,
                        orderBy = filterOptionsModel.OrderBy,
                        searchQuery = filterOptionsModel.SearchQuery,
                        pageNumber = filterOptionsModel.PageNumber,
                        pageSize = filterOptionsModel.PageSize
                    });
            }
        }

        #endregion

    }
}
