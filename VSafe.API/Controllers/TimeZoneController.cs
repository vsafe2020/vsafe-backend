using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
//using VSafe.API.Helpers;
using VSafe.Dto;
using VSafe.Service;
using VSafe.Domain.Entities;
using VSafe.Utility;
using Microsoft.Extensions.Logging;
using IdentityServer4.AccessTokenValidation;

namespace VSafe.API.Controllers
{
    /// <summary>
    /// TimeZone endpoint
    /// </summary>
    [Route("api/timezones")]
    [Produces("application/json")]
    [Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]
    [ApiController]
    public class TimeZoneController : Controller
    {

        #region PRIVATE MEMBERS

        private readonly ITimeZoneService _timezoneService;
        private ILogger<TimeZoneController> _logger;
        private readonly IUrlHelper _urlHelper;

        #endregion


        #region CONSTRUCTOR

        public TimeZoneController(ITimeZoneService timezoneService, ILogger<TimeZoneController> logger, IUrlHelper urlHelper) 
        {
            _logger = logger;
            _timezoneService = timezoneService;
            _urlHelper = urlHelper;
        }

        #endregion


        #region HTTPGET

        [HttpGet(Name = "GetFilteredTimeZones")]
        [Produces("application/vnd.tourmanagement.timezones.hateoas+json", "application/json")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<List<TimeZoneDto>>> GetFilteredTimeZones([FromQuery]FilterOptionsModel filterOptionsModel, [FromHeader(Name = "Accept")] string mediaType)
        {

            //if order by fields are not valid.
            if (!_timezoneService.ValidMappingExists(filterOptionsModel.OrderBy))
            {
                //then return bad request.
                return BadRequest();
            }

            //if fields are not valid.
            if (!VSafeUtils.TypeHasProperties<TimeZoneDto>(filterOptionsModel.Fields))
            {
                //then return bad request.
                return BadRequest();
            }

            //get the paged/filtered show from db. 
            var timezonesFromRepo = await _timezoneService.GetFilteredEntities(filterOptionsModel);

            //if HATEOAS links are required.
            if (mediaType == "application/vnd.tourmanagement.timezones.hateoas+json")
            {
                //create HATEOAS links for each show.
                timezonesFromRepo.ForEach(timezone =>
                {
                    var entityLinks = CreateLinksForTimeZone(timezone.Id, filterOptionsModel.Fields);
                    timezone.links = entityLinks;
                });

                //prepare pagination metadata.
                var paginationMetadata = new
                {
                    totalCount = timezonesFromRepo.TotalCount,
                    pageSize = timezonesFromRepo.PageSize,
                    currentPage = timezonesFromRepo.CurrentPage,
                    totalPages = timezonesFromRepo.TotalPages,
                };

                //add pagination meta data to response header.
                Response.Headers.Add("X-Pagination", JsonConvert.SerializeObject(paginationMetadata));

                //create links for shows.
                var links = CreateLinksForTimeZones(filterOptionsModel, timezonesFromRepo.HasNext, timezonesFromRepo.HasPrevious);

                //prepare model with data and HATEOAS links.
                var linkedCollectionResource = new
                {
                    value = timezonesFromRepo,
                    links = links
                };

                //return the data with Ok response.
                return Ok(linkedCollectionResource);
            }
            else
            {
                var previousPageLink = timezonesFromRepo.HasPrevious ?
                    CreateTimeZonesResourceUri(filterOptionsModel, ResourceUriType.PreviousPage) : null;

                var nextPageLink = timezonesFromRepo.HasNext ?
                    CreateTimeZonesResourceUri(filterOptionsModel, ResourceUriType.NextPage) : null;

                //prepare the pagination metadata.
                var paginationMetadata = new
                {
                    previousPageLink = previousPageLink,
                    nextPageLink = nextPageLink,
                    totalCount = timezonesFromRepo.TotalCount,
                    pageSize = timezonesFromRepo.PageSize,
                    currentPage = timezonesFromRepo.CurrentPage,
                    totalPages = timezonesFromRepo.TotalPages
                };

                //add pagination meta data to response header.
                Response.Headers.Add("X-Pagination", JsonConvert.SerializeObject(paginationMetadata));

                //return the data with Ok response.
                return Ok(timezonesFromRepo);
            }
        }


        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [Produces("application/vnd.tourmanagement.timezones.hateoas+json", "application/json")]
        [HttpGet("{id}", Name = "GetTimeZone")]
        public async Task<ActionResult<Domain.Entities.TimeZone>> GetTimeZone(Guid id, [FromQuery] string fields, [FromHeader(Name = "Accept")] string mediaType)
        {
            object timezoneEntity = null;
            object linkedResourceToReturn = null;

            //if fields are not passed.
            if (string.IsNullOrEmpty(fields))
            {
                _logger.LogInformation("GetTimeZone called");

                //then get the whole entity and map it to the Dto.
                timezoneEntity = Mapper.Map<TimeZoneDto>(await _timezoneService.GetEntityByIdAsync(id));
            }
            else
            {
                //if fields are passed then get the partial show.
                timezoneEntity = await _timezoneService.GetPartialEntityAsync(id, fields);
            }

            //if timezone not found.
            if (timezoneEntity == null)
            {
                //then return not found response.
                return NotFound();
            }

            if (mediaType == "application/vnd.tourmanagement.timezones.hateoas+json")
            {
                //create HATEOS links
                var links = CreateLinksForTimeZone(id, fields);

                //if fields are not passed.
                if (string.IsNullOrEmpty(fields))
                {
                    //convert the typed object to expando object.
                    linkedResourceToReturn = ((TimeZoneDto)timezoneEntity).ShapeData("") as IDictionary<string, object>;

                    //add the HATEOAS links to the model.
                    ((IDictionary<string, object>)linkedResourceToReturn).Add("links", links);
                }
                else
                {
                    linkedResourceToReturn = timezoneEntity;

                    //add the HATEOAS links to the model.
                    ((dynamic)linkedResourceToReturn).links = links;

                }
            }
            else
            {
                linkedResourceToReturn = timezoneEntity;
            }

            //return the Ok response.
            return Ok(linkedResourceToReturn);
        }

        #endregion


        #region HTTPPUT

        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpPut("{id}", Name = "UpdateTimeZone")]
        public async Task<IActionResult> UpdateTimeZone(Guid id, [FromBody]TimeZoneForUpdate TimeZoneForUpdate)
        {

            //if show not found
            if (!await _timezoneService.ExistAsync(x => x.Id == id))
            {
                //then return not found response.
                return NotFound();
            }

            //Update an entity.
            await _timezoneService.UpdateEntityAsync(id, TimeZoneForUpdate);

            //return the response.
            return NoContent();
        }

        #endregion


        #region HTTPPATCH

        [Consumes("application/json-patch+json")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpPatch("{id}")]
        public async Task<IActionResult> PartiallyUpdateTimeZone(Guid id, [FromBody] JsonPatchDocument<TimeZoneForUpdate> jsonPatchDocument)
        {
            TimeZoneForUpdate dto = new TimeZoneForUpdate();
            Domain.Entities.TimeZone timezone = new Domain.Entities.TimeZone();

            //apply the patch changes to the dto. 
            jsonPatchDocument.ApplyTo(dto, ModelState);

            //if the jsonPatchDocument is not valid.
            if (!ModelState.IsValid)
            {
                //then return unprocessableEntity response.
                return new UnprocessableEntityObjectResult(ModelState);
            }

            //if the dto model is not valid after applying changes.
            if (!TryValidateModel(dto))
            {
                //then return unprocessableEntity response.
                return new UnprocessableEntityObjectResult(ModelState);
            }

            //map the chnages from dto to entity.
            Mapper.Map(dto, timezone);

            //set the Id for the show model.
            timezone.Id = id;

            //partially update the chnages to the db. 
            await _timezoneService.UpdatePartialEntityAsync(timezone, jsonPatchDocument);

            //return the response.
            return NoContent();
        }

        #endregion


        #region HTTPPOST

        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpPost(Name = "CreateTimeZone")]
        public async Task<ActionResult<TimeZoneDto>> CreateTimeZone([FromBody]TimeZoneForCreation timezone)
        {
            //create a show in db.
            var timezoneToReturn = await _timezoneService.CreateEntityAsync<TimeZoneDto, TimeZoneForCreation>(timezone);

            //return the show created response.
            return CreatedAtRoute("GetTimeZone", new { id = timezoneToReturn.Id }, timezoneToReturn);
        }

        #endregion


        #region HTTPDELETE
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpDelete("{id}", Name = "DeleteTimeZoneById")]
        public async Task<IActionResult> DeleteTimeZoneById(Guid id)
        {
            //if the timezone exists
            if (await _timezoneService.ExistAsync(x => x.Id == id))
            {
                //delete the timezone from the db.
                await _timezoneService.DeleteEntityAsync(id);
            }
            else
            {
                //if timezone doesn't exists then returns not found.
                return NotFound();
            }

            //return the response.
            return NoContent();
        }

        #endregion


        #region PRIVATE METHODS

        private IEnumerable<LinkDto> CreateLinksForTimeZone(Guid id, string fields)
        {
            var links = new List<LinkDto>();

            if (string.IsNullOrWhiteSpace(fields))
            {
                links.Add(
                  new LinkDto(_urlHelper.Link("GetTimeZone", new { id = id }),
                  "self",
                  "GET"));
            }
            else
            {
                links.Add(
                  new LinkDto(_urlHelper.Link("GetTimeZone", new { id = id, fields = fields }),
                  "self",
                  "GET"));
            }

            links.Add(
              new LinkDto(_urlHelper.Link("DeleteTimeZoneById", new { id = id }),
              "delete_timezone",
              "DELETE"));

            links.Add(
             new LinkDto(_urlHelper.Link("UpdateTimeZone", new { id = id }),
             "update_timezone",
             "PUT"));

            links.Add(
              new LinkDto(_urlHelper.Link("CreateTimeZone", new { }),
              "create_timezone",
              "POST"));

            //links.Add(
            //   new LinkDto(_urlHelper.Link("GetShowsForTour", new { }),
            //   "shows",
            //   "GET"));

            return links;
        }

        private IEnumerable<LinkDto> CreateLinksForTimeZones(FilterOptionsModel filterOptionsModel, bool hasNext, bool hasPrevious)
        {
            var links = new List<LinkDto>();

            links.Add(
               new LinkDto(CreateTimeZonesResourceUri(filterOptionsModel, ResourceUriType.Current), "self", "GET"));

            if (hasNext)
            {
                links.Add(
                  new LinkDto(CreateTimeZonesResourceUri(filterOptionsModel, ResourceUriType.NextPage), "nextPage", "GET"));
            }

            if (hasPrevious)
            {
                links.Add(new LinkDto(CreateTimeZonesResourceUri(filterOptionsModel, ResourceUriType.PreviousPage), "previousPage", "GET"));
            }

            return links;
        }

        private string CreateTimeZonesResourceUri(FilterOptionsModel filterOptionsModel, ResourceUriType type)
        {
            switch (type)
            {
                case ResourceUriType.PreviousPage:
                    return _urlHelper.Link("GetFilteredTimeZones",
                      new
                      {
                          fields = filterOptionsModel.Fields,
                          orderBy = filterOptionsModel.OrderBy,
                          searchQuery = filterOptionsModel.SearchQuery,
                          pageNumber = filterOptionsModel.PageNumber - 1,
                          pageSize = filterOptionsModel.PageSize
                      });
                case ResourceUriType.NextPage:
                    return _urlHelper.Link("GetFilteredTimeZones",
                      new
                      {
                          fields = filterOptionsModel.Fields,
                          orderBy = filterOptionsModel.OrderBy,
                          searchQuery = filterOptionsModel.SearchQuery,
                          pageNumber = filterOptionsModel.PageNumber + 1,
                          pageSize = filterOptionsModel.PageSize
                      });

                default:
                    return _urlHelper.Link("GetFilteredTimeZones",
                    new
                    {
                        fields = filterOptionsModel.Fields,
                        orderBy = filterOptionsModel.OrderBy,
                        searchQuery = filterOptionsModel.SearchQuery,
                        pageNumber = filterOptionsModel.PageNumber,
                        pageSize = filterOptionsModel.PageSize
                    });
            }
        }

        #endregion

    }
}
