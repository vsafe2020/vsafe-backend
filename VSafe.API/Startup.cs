using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.AspNetCore.Mvc.Routing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System.IO;
using VSafe.Domain;
using VSafe.Domain.Entities;
using VSafe.Dto;
using VSafe.Repository;
using VSafe.Service;
using Swashbuckle.AspNetCore.SwaggerUI;
//using Microsoft.OpenApi.Models;
using System;
using System.Reflection;
using System.Linq;
using Microsoft.AspNetCore.Mvc.Formatters;
using IdentityServer4.AccessTokenValidation;
using IdentityModel.AspNetCore.OAuth2Introspection;
using System.Collections.Generic;
using Swashbuckle.AspNetCore.Swagger;

namespace VSafe.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
         
            services.AddMvc(setupAction => 
            {
                setupAction.ReturnHttpNotAcceptable = true;

                var jsonOutputFormatter = setupAction.OutputFormatters
               .OfType<JsonOutputFormatter>().FirstOrDefault();

                if (jsonOutputFormatter != null)
                {
                   jsonOutputFormatter.SupportedMediaTypes.Add("application/vnd.VSafe.ExternalExposerss.hateoas + json");
                   jsonOutputFormatter.SupportedMediaTypes.Add("application/vnd.VSafe.ExternalContacts.hateoas + json");
                   jsonOutputFormatter.SupportedMediaTypes.Add("application/vnd.VSafe.EmployeeExposerss.hateoas + json");
                   jsonOutputFormatter.SupportedMediaTypes.Add("application/vnd.VSafe.Employees.hateoas + json");
                   jsonOutputFormatter.SupportedMediaTypes.Add("application/vnd.VSafe.TimeZones.hateoas + json");
                   jsonOutputFormatter.SupportedMediaTypes.Add("application/vnd.VSafe.Departments.hateoas + json");
                   jsonOutputFormatter.SupportedMediaTypes.Add("application/vnd.VSafe.Accounts.hateoas + json");
                    jsonOutputFormatter.SupportedMediaTypes.Add("application/vnd.tourmanagement.locations.hateoas+json");
                }

            }).SetCompatibilityVersion(CompatibilityVersion.Version_2_1)
             .AddJsonOptions(options =>
             {
                 options.SerializerSettings.DateParseHandling = DateParseHandling.DateTimeOffset;
                 options.SerializerSettings.ContractResolver =
                     new CamelCasePropertyNamesContractResolver();
             });

            #region CORS POLICY

            // Configure CORS so the API allows requests from JavaScript.  
            // For demo purposes, all origins/headers/methods are allowed.  
            services.AddCors(options =>
            {
                options.AddPolicy("AllowAllOriginsHeadersAndMethods",
                    builder => builder.AllowAnyOrigin().AllowAnyHeader().AllowAnyMethod());
            });

            #endregion

            #region REGISTER DB-CONTEXT

            var connectionString = Configuration["ConnectionStrings:VSafeDB"];
            services.AddDbContext<VSafeContext>(o => o.UseSqlServer(connectionString));

            #endregion

            // register the repository
           services.AddScoped<IAccountService, AccountService>();
           services.AddScoped<IAccountRepository,AccountRepository>();
           services.AddScoped<IDepartmentService, DepartmentService>();
           services.AddScoped<IDepartmentRepository,DepartmentRepository>();
           services.AddScoped<ITimeZoneService, TimeZoneService>();
           services.AddScoped<ITimeZoneRepository,TimeZoneRepository>();
           services.AddScoped<IEmployeeService, EmployeeService>();
           services.AddScoped<IEmployeeRepository,EmployeeRepository>();
           services.AddScoped<IEmployeeExposersService, EmployeeExposersService>();
           services.AddScoped<IEmployeeExposersRepository,EmployeeExposersRepository>();
           services.AddScoped<IExternalContactService, ExternalContactService>();
           services.AddScoped<IExternalContactRepository,ExternalContactRepository>();
           services.AddScoped<IExternalExposersService, ExternalExposersService>();
           services.AddScoped<IExternalExposersRepository,ExternalExposersRepository>();
            services.AddScoped(typeof(IRepository<,>), typeof(Repository<,>));

            // register the repository
            services.AddScoped<ILocationRepository,LocationRepository>();

            // register an IHttpContextAccessor so we can access the current
            // HttpContext in services by injecting it
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            // register the user info service
            services.AddScoped<IUserInfoService, UserInfoService>();

            // register the user info service
            services.AddScoped<ILocationService, LocationService>();

            services.AddSingleton<IActionContextAccessor, ActionContextAccessor>();

            services.AddScoped<IUrlHelper, UrlHelper>(implementationFactory =>
            {
                var actionContext = implementationFactory.GetService<IActionContextAccessor>().ActionContext;
                return new UrlHelper(actionContext);
            });

            var identityServerUrl = Configuration["IdentityServerUrl"];

            services.AddAuthentication(IdentityServerAuthenticationDefaults.AuthenticationScheme)
                .AddIdentityServerAuthentication(options =>
                {
                    options.Authority = identityServerUrl;
                    //  options.RequireHttpsMetadata = false; // only for development
                    options.ApiName = "vsafeapi";
                    options.ApiSecret = "secret";
                    options.EnableCaching = true;
                    options.CacheDuration = TimeSpan.FromMinutes(1); // that's the default
                    options.TokenRetriever = new Func<HttpRequest, string>(req =>
                    {
                        var fromHeader = TokenRetrieval.FromAuthorizationHeader();
                        var fromQuery = TokenRetrieval.FromQueryString();
                        return fromHeader(req) ?? fromQuery(req);
                    });
                });

            services.AddSwaggerGen(setupAction =>
            {
                setupAction.SwaggerDoc("VSafeAPISpecification", new Info
                {
                    Title = "VSafe API",
                    Version = "1",
                    Description = "Through this api you can access VSafe related entities.",
                    //Contact = new OpenApiContact
                    //{
                    //    Email = "jignesh@abhisi.com",
                    //    Name = "Jignesh Mistry",
                    //    Url = new Uri("https://www.twitter.com/jigneshmistry89")
                    //},
                    //License = new OpenApiLicense
                    //{
                    //    Name = "MIT License",
                    //    Url = new Uri("https://opensource.org/licenses/MIT")
                    //}
                });

                var xmlCommentFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlCommentDtoFile = "VSafe.Dto.xml";
                var xmlCommentUtilFile = "VSafe.Utility.xml";
                var xmlCommentFullPath = Path.Combine(AppContext.BaseDirectory, xmlCommentFile);
                var xmlCommentDtoFullPath = Path.Combine(AppContext.BaseDirectory, xmlCommentDtoFile);
                var xmlCommentUtilFullPath = Path.Combine(AppContext.BaseDirectory, xmlCommentUtilFile);
                setupAction.IncludeXmlComments(xmlCommentFullPath);
                setupAction.IncludeXmlComments(xmlCommentDtoFullPath);
                setupAction.IncludeXmlComments(xmlCommentUtilFullPath);

                setupAction.AddSecurityDefinition("oauth2", new OAuth2Scheme
                {
                    Description = "Standard Authorization header using the Bearer scheme. Example: \"bearer {token}\"",
                    Type = "oauth2",
                    Flow = "implicit",
                    AuthorizationUrl = identityServerUrl + "/connect/authorize",
                    TokenUrl = identityServerUrl + "/connect/token",
                    Scopes = new Dictionary<string, string> {
                        { "vsafeapi", "VSafe API" }
                    }
                });
                setupAction.AddSecurityRequirement(new Dictionary<string, IEnumerable<string>>
                {
                    { "oauth2", new[] { "vsafeapi" } }
                });

            });


            services.Configure<ApiBehaviorOptions>(options =>
            {
                options.InvalidModelStateResponseFactory = actionContext =>
                {
                    var actionExecutingContext =
                        actionContext as Microsoft.AspNetCore.Mvc.Filters.ActionExecutingContext;

                    // if there are modelstate errors & all keys were correctly
                    // found/parsed we're dealing with validation errors
                    if (actionContext.ModelState.ErrorCount > 0
                        && actionExecutingContext?.ActionArguments.Count == actionContext.ActionDescriptor.Parameters.Count)
                    {
                        return new UnprocessableEntityObjectResult(actionContext.ModelState);
                    }

                    // if one of the keys wasn't correctly found / couldn't be parsed
                    // we're dealing with null/unparsable input
                    return new BadRequestObjectResult(actionContext.ModelState);
                };
            });

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory, VSafeContext context)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();                
            }
            else
            {
                //TODO::
                app.UseExceptionHandler(appBuilder =>
                {
                    appBuilder.Run(async (ctx) =>
                    {
                        var exceptionHandlerFeature = ctx.Features.Get<IExceptionHandlerFeature>();
                        if (exceptionHandlerFeature != null)
                        {
                            var logger = loggerFactory.CreateLogger("Global exception logger");
                            logger.LogError(500,
                                exceptionHandlerFeature.Error,
                                exceptionHandlerFeature.Error.Message);
                        }

                        ctx.Response.StatusCode = 500;
                        await ctx.Response.WriteAsync("An unexpected fault happened. Try again later.");
                    });
                });

                app.UseHsts();
            }

            AutoMapper.Mapper.Initialize(config =>
            {

               config.CreateMap<ExternalExposersForUpdate, ExternalExposer>();
               config.CreateMap<ExternalExposersForCreation, ExternalExposer>();
               config.CreateMap<ExternalExposer, ExternalExposersDto>();
               config.CreateMap<ExternalContactForUpdate, ExternalContact>();
               config.CreateMap<ExternalContactForCreation, ExternalContact>();
               config.CreateMap<ExternalContact, ExternalContactDto>();
               config.CreateMap<EmployeeExposersForUpdate, EmployeeExposer>();
               config.CreateMap<EmployeeExposersForCreation, EmployeeExposer>();
               config.CreateMap<EmployeeExposer, EmployeeExposersDto>();
               config.CreateMap<EmployeeForUpdate, Employee>();
               config.CreateMap<EmployeeForCreation, Employee>();
               config.CreateMap<Employee, EmployeeDto>();
               config.CreateMap<TimeZoneForUpdate, Domain.Entities.TimeZone>();
               config.CreateMap<TimeZoneForCreation, Domain.Entities.TimeZone>();
               config.CreateMap<Domain.Entities.TimeZone, TimeZoneDto>();
               config.CreateMap<DepartmentForUpdate, Department>();
               config.CreateMap<DepartmentForCreation, Department>();
               config.CreateMap<Department, DepartmentDto>();
               config.CreateMap<AccountForUpdate, Account>();
               config.CreateMap<AccountForCreation, Account>();
               config.CreateMap<Account, AccountDto>();
                config.CreateMap<Location, LocationDto>();
                config.CreateMap<LocationForCreation, Location>();
                config.CreateMap<LocationForUpdate, Location>();

            });

            // Enable CORS
            app.UseCors("AllowAllOriginsHeadersAndMethods");

            app.UseHttpsRedirection();

            app.UseSwagger();
            var hostedURL = Configuration["HostedUrl"];
            app.UseSwaggerUI(setupAction =>
            {
                setupAction.SwaggerEndpoint("/swagger/VSafeAPISpecification/swagger.json", "Tourmanagement API");
                setupAction.RoutePrefix = "";
                setupAction.OAuthClientId("swaggerui");
                setupAction.OAuth2RedirectUrl(hostedURL + "oauth2-redirect.html");
                setupAction.DocExpansion(DocExpansion.None);
                setupAction.EnableDeepLinking();
                setupAction.DisplayOperationId();
            });
           
            app.UseMvc();

            //Add the seed data to the database.
            context.EnsureSeedDataForContext();

        }
    }
}
