﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TourManagement.API.Helpers;
using TourManagement.Dtos;
using TourManagement.Service;

namespace TourManagement.API.Controllers
{
    [ApiExplorerSettings(IgnoreApi =true)]
    [Route("api/tours/{tourId}/showcollections")]
    //[Authorize]
    public class ShowCollectionsController : Controller
    {

        #region PRIVATE MEMBERS

        private readonly ITourService _tourService;
        private readonly IShowService _showService;

        #endregion


        #region CONSTRUCTOR

        public ShowCollectionsController(ITourService tourService,
            IShowService showService)
        {
            _tourService = tourService;
            _showService = showService;
        }

        #endregion


        #region HTTPGET

        /// <summary>
        /// Get the shows by the showIds
        /// </summary>
        /// <param name="tourId">Unique identifier for the tour.</param>
        /// <param name="showIds">Unique identifier for the show.</param>
        /// <returns>show list</returns>
        [HttpGet("({showIds})", Name = "GetShowCollection")]
        [RequestHeaderMatchesMediaType("Accept", new[] { "application/json", "application/vnd.marvin.showcollection+json" })]
        public async Task<IActionResult> GetShowCollection(Guid tourId, [ModelBinder(BinderType = typeof(ArrayModelBinder))] IEnumerable<Guid> showIds)
        {
            //if showIds not provoded.
            if (showIds == null || !showIds.Any())
            {
                //then return bad request.
                return BadRequest();
            }

            //if the tour doesn't exists.
            if (!await _tourService.ExistAsync(x => x.TourId == tourId))
            {
                //return not found response.
                return NotFound();
            }

            //get the shows from the db.
            var showEntities = _showService.GetPartialEntities(x => showIds.Contains(x.ShowId), "ShowId,Venue,City,Country");

            //map the show to show dto.
            var showsDto = Mapper.Map<List<Show>>(showEntities);
           
            //return record not found.
            if (showIds.Count() != ((List<Show>)showsDto).Count())
            {
                //return not found response.
                return NotFound();
            }

            //return the data with Ok response.
            return Ok(showsDto);
        }

        #endregion


        #region HTTPPOST

        /// <summary>
        /// Creates a collection of shows.
        /// </summary>
        /// <param name="tourId">Unique identifier for the show.</param>
        /// <param name="showCollection">Collection of Show Dto.</param>
        /// <returns>Collection of Shows</returns>
        [HttpPost]
        [RequestHeaderMatchesMediaType("Content-Type", new[] { "application/json","application/vnd.marvin.showcollectionforcreation+json" })]
        public async Task<IActionResult> CreateShowCollection(Guid tourId,[FromBody] IEnumerable<ShowForCreation> showCollection)
        {
            //show collection not defined.
            if (showCollection == null)
            {
                //return bad request response.
                return BadRequest();
            }

            //tour doesn't exists.
            if (!await _tourService.ExistAsync(x => x.TourId == tourId))
            {
                //return not found response.
                return NotFound();
            }

            //map the shows dto to shows entity. 
            var showEntities = Mapper.Map<List<Domain.Entities.Show>>(showCollection);

            //set the tourId in the entity.
            showEntities.ForEach(x => x.TourId = tourId);

            //add the show entity to the db context.
            _showService.CreateBulkEntity(showEntities);

            //save the changes to the db.
            if (!await _showService.SaveChangesAsync())
            {
                //save failed then throw exception.
                throw new Exception("Adding a collection of shows failed on save.");
            }

            //map the show entity to the show dto.
            var showCollectionToReturn = Mapper.Map<IEnumerable<Show>>(showEntities);

            var showIdsAsString = string.Join(",", showCollectionToReturn.Select(a => a.ShowId));

            //return the created response.
            return CreatedAtRoute("GetShowCollection",new { tourId, showIds = showIdsAsString },showCollectionToReturn);
        }

        #endregion


        #region HTTPDELETE

        /// <summary>
        /// Deletes the shows.
        /// </summary>
        /// <param name="tourId">Unique identifier for the show</param>
        /// <param name="showIds">Collection of showIds</param>
        /// <returns></returns>
        [HttpDelete("({showIds})", Name = "DeleteShowCollection")]
        public async Task<IActionResult> DeleteShowCollection(Guid tourId, [ModelBinder(BinderType = typeof(ArrayModelBinder))] IEnumerable<Guid> showIds)
        {
            //if showIds not provoded.
            if (showIds == null || !showIds.Any())
            {
                //then return bad request.
                return BadRequest();
            }

            //if the tour doesn't exists.
            if (!await _tourService.ExistAsync(x => x.TourId == tourId))
            {
                //return not found response.
                return NotFound();
            }

            //delete the shows from db.
            await _showService.DeleteBulkEntityAsync(x => showIds.Contains(x.ShowId));

            //return the response.
            return NoContent();
        }

        #endregion

    }
}
