﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using TourManagement.Domain.Entities;

namespace TourManagement.Domain
{
    public class TourManagementContext : DbContext
    {

        #region PUBLIC MEMBERS

        public DbSet<Band> Bands { get; set; }

        private readonly IUserInfoService _userInfoService;

        #endregion

        #region CONSTRUCTOR

        public TourManagementContext(DbContextOptions<TourManagementContext> options, IUserInfoService userInfoService)
         : base(options)
        {
            // userInfoService is a required argument
            _userInfoService = userInfoService ?? throw new ArgumentNullException(nameof(userInfoService));
        }

        #endregion


        public override Task<int> SaveChangesAsync(CancellationToken cancellationToken = default(CancellationToken))
        {
            // get added or updated entries
            var addedOrUpdatedEntries = ChangeTracker.Entries()
                    .Where(x => (x.State == EntityState.Added || x.State == EntityState.Modified));

            // fill out the audit fields
            foreach (var entry in addedOrUpdatedEntries)
            {
                var entity = entry.Entity as AuditableEntity;

                if (entry.State == EntityState.Added)
                {
                    if (_userInfoService.UserId == null)
                    {
                        entity.CreatedBy = "system";
                    }
                    else
                    {
                        entity.CreatedBy = _userInfoService.UserId;
                    }
                    entity.CreatedOn = DateTime.UtcNow;
                }

                entity.UpdatedBy = _userInfoService.UserId;
                entity.UpdatedOn = DateTime.UtcNow;
            }
            
            return base.SaveChangesAsync(cancellationToken);
        }
    }
}
