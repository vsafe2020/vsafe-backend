﻿using System;
using System.Collections.Generic;
using TourManagement.Domain;
using TourManagement.Domain.Entities;
using TourManagement.Dto;
using Microsoft.Extensions.Logging;

namespace TourManagement.Service
{
    public class BandService : ServiceBase<Band, Guid>, IBandService
    {

        #region PRIVATE MEMBERS

        private readonly IBandRepository _bandRepository;

        #endregion


        #region CONSTRUCTOR

        public BandService(IBandRepository bandRepository, ILogger<BandService> logger) : base(bandRepository, logger)
        {
            _bandRepository = bandRepository;
        }

        #endregion


        #region PUBLIC MEMBERS   


        #endregion


        #region OVERRIDDEN IMPLEMENTATION

        public override Dictionary<string, PropertyMappingValue> GetPropertyMapping()
        {
            return new Dictionary<string, PropertyMappingValue>(StringComparer.OrdinalIgnoreCase)
                    {
                        { "Id", new PropertyMappingValue(new List<string>() { "Id" } ) },
                        { "Name", new PropertyMappingValue(new List<string>() { "Name" } )},
                    };
        }

        public override string GetDefaultOrderByColumn()
        {
            return "UpdatedOn";
        }

        public override string GetDefaultFieldsToSelect()
        {
            return "Id,Name";
        }

        #endregion
    }
}
