﻿using System;
using System.Threading.Tasks;
using TourManagement.Dtos;

namespace TourManagement.Service
{
    public interface IBandService : IService<Band, Guid>
    {

    }
}
