﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
//using TourManagement.API.Helpers;
using TourManagement.Dto;
using TourManagement.Service;
using TourManagement.Domain.Entities;
using TourManagement.Utility;
using Microsoft.Extensions.Logging;

namespace TourManagement.API.Controllers
{
    /// <summary>
    /// Band endpoint
    /// </summary>
    [Route("api/bands")]
    [Produces("application/json")]
    [ApiController]
    public class BandController : Controller
    {

        #region PRIVATE MEMBERS

        private readonly IBandService _bandService;
        private ILogger<BandController> _logger;
        private readonly IUrlHelper _urlHelper;

        #endregion


        #region CONSTRUCTOR

        public BandController(IBandService bandService, ILogger<BandController> logger, IUrlHelper urlHelper) 
        {
            _logger = logger;
            _bandService = bandService;
            _urlHelper = urlHelper;
        }

        #endregion


        #region HTTPGET

        [HttpGet(Name = "GetFilteredBands")]
        [Produces("application/vnd.tourmanagement.bands.hateoas+json", "application/json")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<List<BandDto>>> GetFilteredBands([FromQuery]FilterOptionsModel filterOptionsModel, [FromHeader(Name = "Accept")] string mediaType)
        {

            //if order by fields are not valid.
            if (!_bandService.ValidMappingExists(filterOptionsModel.OrderBy))
            {
                //then return bad request.
                return BadRequest();
            }

            //if fields are not valid.
            if (!TourManagementUtils.TypeHasProperties<BandDto>(filterOptionsModel.Fields))
            {
                //then return bad request.
                return BadRequest();
            }

            //get the paged/filtered show from db. 
            var bandsFromRepo = await _bandService.GetFilteredEntities(filterOptionsModel);

            //if HATEOAS links are required.
            if (mediaType == "application/vnd.tourmanagement.bands.hateoas+json")
            {
                //create HATEOAS links for each show.
                bandsFromRepo.ForEach(band =>
                {
                    var entityLinks = CreateLinksForBand(band.Id, filterOptionsModel.Fields);
                    band.links = entityLinks;
                });

                //prepare pagination metadata.
                var paginationMetadata = new
                {
                    totalCount = bandsFromRepo.TotalCount,
                    pageSize = bandsFromRepo.PageSize,
                    currentPage = bandsFromRepo.CurrentPage,
                    totalPages = bandsFromRepo.TotalPages,
                };

                //add pagination meta data to response header.
                Response.Headers.Add("X-Pagination", JsonConvert.SerializeObject(paginationMetadata));

                //create links for shows.
                var links = CreateLinksForBands(filterOptionsModel, bandsFromRepo.HasNext, bandsFromRepo.HasPrevious);

                //prepare model with data and HATEOAS links.
                var linkedCollectionResource = new
                {
                    value = bandsFromRepo,
                    links = links
                };

                //return the data with Ok response.
                return Ok(linkedCollectionResource);
            }
            else
            {
                var previousPageLink = bandsFromRepo.HasPrevious ?
                    CreateBandsResourceUri(filterOptionsModel, ResourceUriType.PreviousPage) : null;

                var nextPageLink = bandsFromRepo.HasNext ?
                    CreateBandsResourceUri(filterOptionsModel, ResourceUriType.NextPage) : null;

                //prepare the pagination metadata.
                var paginationMetadata = new
                {
                    previousPageLink = previousPageLink,
                    nextPageLink = nextPageLink,
                    totalCount = bandsFromRepo.TotalCount,
                    pageSize = bandsFromRepo.PageSize,
                    currentPage = bandsFromRepo.CurrentPage,
                    totalPages = bandsFromRepo.TotalPages
                };

                //add pagination meta data to response header.
                Response.Headers.Add("X-Pagination", JsonConvert.SerializeObject(paginationMetadata));

                //return the data with Ok response.
                return Ok(bandsFromRepo);
            }
        }


        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [Produces("application/vnd.tourmanagement.bands.hateoas+json", "application/json")]
        [HttpGet("{id}", Name = "GetBand")]
        public async Task<ActionResult<Band>> GetBand(Guid id, [FromQuery] string fields, [FromHeader(Name = "Accept")] string mediaType)
        {
            object bandEntity = null;
            object linkedResourceToReturn = null;

            //if fields are not passed.
            if (string.IsNullOrEmpty(fields))
            {
                _logger.LogInformation("GetBand called");

                //then get the whole entity and map it to the Dto.
                bandEntity = Mapper.Map<BandDto>(await _bandService.GetEntityByIdAsync(id));
            }
            else
            {
                //if fields are passed then get the partial show.
                bandEntity = await _bandService.GetPartialEntityAsync(id, fields);
            }

            //if band not found.
            if (bandEntity == null)
            {
                //then return not found response.
                return NotFound();
            }

            if (mediaType == "application/vnd.tourmanagement.bands.hateoas+json")
            {
                //create HATEOS links
                var links = CreateLinksForBand(id, fields);

                //if fields are not passed.
                if (string.IsNullOrEmpty(fields))
                {
                    //convert the typed object to expando object.
                    linkedResourceToReturn = ((BandDto)bandEntity).ShapeData("") as IDictionary<string, object>;

                    //add the HATEOAS links to the model.
                    ((IDictionary<string, object>)linkedResourceToReturn).Add("links", links);
                }
                else
                {
                    linkedResourceToReturn = bandEntity;

                    //add the HATEOAS links to the model.
                    ((dynamic)linkedResourceToReturn).links = links;

                }
            }
            else
            {
                linkedResourceToReturn = bandEntity;
            }

            //return the Ok response.
            return Ok(linkedResourceToReturn);
        }

        #endregion


        #region HTTPPUT

        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpPut("{id}", Name = "UpdateBand")]
        public async Task<IActionResult> UpdateBand(Guid id, [FromBody]BandForUpdate BandForUpdate)
        {

            //if show not found
            if (!await _bandService.ExistAsync(x => x.Id == id))
            {
                //then return not found response.
                return NotFound();
            }

            //Update an entity.
            await _bandService.UpdateEntityAsync(id, BandForUpdate);

            //return the response.
            return NoContent();
        }

        #endregion


        #region HTTPPATCH

        [Consumes("application/json-patch+json")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpPatch("{id}")]
        public async Task<IActionResult> PartiallyUpdateBand(Guid id, [FromBody] JsonPatchDocument<BandForUpdate> jsonPatchDocument)
        {
            BandForUpdate dto = new BandForUpdate();
            Band band = new Band();

            //apply the patch changes to the dto. 
            jsonPatchDocument.ApplyTo(dto, ModelState);

            //if the jsonPatchDocument is not valid.
            if (!ModelState.IsValid)
            {
                //then return unprocessableEntity response.
                return new UnprocessableEntityObjectResult(ModelState);
            }

            //if the dto model is not valid after applying changes.
            if (!TryValidateModel(dto))
            {
                //then return unprocessableEntity response.
                return new UnprocessableEntityObjectResult(ModelState);
            }

            //map the chnages from dto to entity.
            Mapper.Map(dto, band);

            //set the Id for the show model.
            band.Id = id;

            //partially update the chnages to the db. 
            await _bandService.UpdatePartialEntityAsync(band, jsonPatchDocument);

            //return the response.
            return NoContent();
        }

        #endregion


        #region HTTPPOST

        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpPost(Name = "CreateBand")]
        public async Task<ActionResult<BandDto>> CreateBand([FromBody]BandForCreation band)
        {
            //create a show in db.
            var bandToReturn = await _bandService.CreateEntityAsync<BandDto, BandForCreation>(band);

            //return the show created response.
            return CreatedAtRoute("GetBand", new { id = bandToReturn.Id }, bandToReturn);
        }

        #endregion


        #region HTTPDELETE
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpDelete("{id}", Name = "DeleteBandById")]
        public async Task<IActionResult> DeleteBandById(Guid id)
        {
            //if the band exists
            if (await _bandService.ExistAsync(x => x.Id == id))
            {
                //delete the band from the db.
                await _bandService.DeleteEntityAsync(id);
            }
            else
            {
                //if band doesn't exists then returns not found.
                return NotFound();
            }

            //return the response.
            return NoContent();
        }

        #endregion


        #region PRIVATE METHODS

        private IEnumerable<LinkDto> CreateLinksForBand(Guid id, string fields)
        {
            var links = new List<LinkDto>();

            if (string.IsNullOrWhiteSpace(fields))
            {
                links.Add(
                  new LinkDto(_urlHelper.Link("GetBand", new { id = id }),
                  "self",
                  "GET"));
            }
            else
            {
                links.Add(
                  new LinkDto(_urlHelper.Link("GetBand", new { id = id, fields = fields }),
                  "self",
                  "GET"));
            }

            links.Add(
              new LinkDto(_urlHelper.Link("DeleteBandById", new { id = id }),
              "delete_band",
              "DELETE"));

            links.Add(
             new LinkDto(_urlHelper.Link("UpdateBand", new { id = id }),
             "update_band",
             "PUT"));

            links.Add(
              new LinkDto(_urlHelper.Link("CreateBand", new { }),
              "create_band",
              "POST"));

            //links.Add(
            //   new LinkDto(_urlHelper.Link("GetShowsForTour", new { }),
            //   "shows",
            //   "GET"));

            return links;
        }

        private IEnumerable<LinkDto> CreateLinksForBands(FilterOptionsModel filterOptionsModel, bool hasNext, bool hasPrevious)
        {
            var links = new List<LinkDto>();

            links.Add(
               new LinkDto(CreateBandsResourceUri(filterOptionsModel, ResourceUriType.Current), "self", "GET"));

            if (hasNext)
            {
                links.Add(
                  new LinkDto(CreateBandsResourceUri(filterOptionsModel, ResourceUriType.NextPage), "nextPage", "GET"));
            }

            if (hasPrevious)
            {
                links.Add(new LinkDto(CreateBandsResourceUri(filterOptionsModel, ResourceUriType.PreviousPage), "previousPage", "GET"));
            }

            return links;
        }

        private string CreateBandsResourceUri(FilterOptionsModel filterOptionsModel, ResourceUriType type)
        {
            switch (type)
            {
                case ResourceUriType.PreviousPage:
                    return _urlHelper.Link("GetFilteredBands",
                      new
                      {
                          fields = filterOptionsModel.Fields,
                          orderBy = filterOptionsModel.OrderBy,
                          searchQuery = filterOptionsModel.SearchQuery,
                          pageNumber = filterOptionsModel.PageNumber - 1,
                          pageSize = filterOptionsModel.PageSize
                      });
                case ResourceUriType.NextPage:
                    return _urlHelper.Link("GetFilteredBands",
                      new
                      {
                          fields = filterOptionsModel.Fields,
                          orderBy = filterOptionsModel.OrderBy,
                          searchQuery = filterOptionsModel.SearchQuery,
                          pageNumber = filterOptionsModel.PageNumber + 1,
                          pageSize = filterOptionsModel.PageSize
                      });

                default:
                    return _urlHelper.Link("GetFilteredBands",
                    new
                    {
                        fields = filterOptionsModel.Fields,
                        orderBy = filterOptionsModel.OrderBy,
                        searchQuery = filterOptionsModel.SearchQuery,
                        pageNumber = filterOptionsModel.PageNumber,
                        pageSize = filterOptionsModel.PageSize
                    });
            }
        }

        #endregion

    }
}
