﻿using System;
using TourManagement.Domain.Entities;

namespace TourManagement.Domain
{
    public interface IBandRepository : IRepository<Band, Guid>
    {

    }
}
