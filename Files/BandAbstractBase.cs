﻿using System;

namespace TourManagement.Dto
{
    public abstract class BandAbstractBase
    {
        /// <summary>
        /// Band Id.
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Band Name.
        /// </summary>
        public string Name { get; set; }

    }
}
