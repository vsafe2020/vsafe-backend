﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using TourManagement.Domain;
using TourManagement.Domain.Entities;
using Microsoft.Extensions.Logging;

namespace TourManagement.Repository
{
    public class BandRepository : Repository<Band, Guid>, IBandRepository
    {
        #region PRIVATE VARIABLE

        private readonly DbSet<Band> _dbset;
        private readonly TourManagementContext _context;

        #endregion

        #region CONSTRUCTOR

        public BandRepository(TourManagementContext context, ILogger<BandRepository> logger) : base(context, logger)
        {
            _context = context;
            _dbset = context.Set<Band>();
        }

        #endregion

        #region OVERRIDDEN IMPLEMENTATION

        public override string GetPrimaryKeyColumnName()
        {
            return "Id";
        }

        public override IQueryable<Band> GetFilteredEntities(bool bIsAsTrackable = false)
        {
            return _dbset;
        }

        #endregion

    }
}
