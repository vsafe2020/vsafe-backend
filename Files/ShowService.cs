﻿using System;
using System.Collections.Generic;
using TourManagement.Domain;
using TourManagement.Utility;

namespace TourManagement.Service
{
    public class ShowService : ServiceBase<Domain.Entities.Show, Guid>, IShowService
    {

        #region PRIVATE MEMBERS

        private readonly IShowRepository _showRepository;
        private readonly IPropertyMappingService _propertyMappingService;

        #endregion


        #region CONSTRUCTOR

        public ShowService(IShowRepository showRepository, 
            IPropertyMappingService propertyMappingService) : base(showRepository)
        {
            _showRepository = showRepository;
            _propertyMappingService = propertyMappingService;
        }

        #endregion


        #region PUBLIC MEMBERS   


        #endregion


        #region OVERRIDDEN IMPLEMENTATION

        public override Dictionary<string, PropertyMappingValue> GetPropertyMapping()
        {
            return _propertyMappingService.GetPropertyMapping<Dtos.Show, Domain.Entities.Show>();
        }

        public override string GetDefaultOrderByColumn()
        {
            return "UpdatedOn";
        }

        public override string GetDefaultFieldsToSelect()
        {
            return "ShowId,Venue,City,Country";
        }

        #endregion
    }
}
