﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.AspNetCore.Mvc.Routing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System.IO;
using TourManagement.Domain;
using TourManagement.Domain.Entities;
using TourManagement.Dto;
using TourManagement.Repository;
using TourManagement.Service;
using Swashbuckle.AspNetCore.SwaggerUI;
using Microsoft.OpenApi.Models;
using System;
using System.Reflection;
using System.Linq;
using Microsoft.AspNetCore.Mvc.Formatters;


namespace TourManagement.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
         
            services.AddMvc(setupAction => 
            {
                setupAction.ReturnHttpNotAcceptable = true;

                var jsonOutputFormatter = setupAction.OutputFormatters
               .OfType<JsonOutputFormatter>().FirstOrDefault();

                if (jsonOutputFormatter != null)
                {
                    jsonOutputFormatter.SupportedMediaTypes.Add("application/vnd.tourmanagement.bands.hateoas+json");
                }

            }).SetCompatibilityVersion(CompatibilityVersion.Version_2_1)
             .AddJsonOptions(options =>
             {
                 options.SerializerSettings.DateParseHandling = DateParseHandling.DateTimeOffset;
                 options.SerializerSettings.ContractResolver =
                     new CamelCasePropertyNamesContractResolver();
             });

            #region CORS POLICY

            // Configure CORS so the API allows requests from JavaScript.  
            // For demo purposes, all origins/headers/methods are allowed.  
            services.AddCors(options =>
            {
                options.AddPolicy("AllowAllOriginsHeadersAndMethods",
                    builder => builder.AllowAnyOrigin().AllowAnyHeader().AllowAnyMethod());
            });

            #endregion

            #region REGISTER DB-CONTEXT

            var connectionString = Configuration["ConnectionStrings:TourManagementDB"];
            services.AddDbContext<TourManagementContext>(o => o.UseSqlServer(connectionString));

            #endregion

            // register the repository
            services.AddScoped(typeof(IRepository<,>), typeof(Repository<,>));

            // register the repository
            services.AddScoped<IBandRepository,BandRepository>();

            // register an IHttpContextAccessor so we can access the current
            // HttpContext in services by injecting it
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            // register the user info service
            services.AddScoped<IUserInfoService, UserInfoService>();

            // register the user info service
            services.AddScoped<IBandService, BandService>();

            services.AddSingleton<IActionContextAccessor, ActionContextAccessor>();

            services.AddScoped<IUrlHelper, UrlHelper>(implementationFactory =>
            {
                var actionContext = implementationFactory.GetService<IActionContextAccessor>().ActionContext;
                return new UrlHelper(actionContext);
            });

            services.AddSwaggerGen(setupAction =>
            {
                setupAction.SwaggerDoc("TourManagementAPISpecification", new Microsoft.OpenApi.Models.OpenApiInfo
                {
                    Title = "Tourmanagement API",
                    Version = "1",
                    Description = "Through this api you can access tourmanagement related entities.",
                    Contact = new OpenApiContact
                    {
                        Email = "jignesh@abhisi.com",
                        Name = "Jignesh Mistry",
                        Url = new Uri("https://www.twitter.com/jigneshmistry89")
                    },
                    License = new OpenApiLicense
                    {
                        Name = "MIT License",
                        Url = new Uri("https://opensource.org/licenses/MIT")
                    }
                });

                var xmlCommentFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlCommentDtoFile = "TourManagement.Dto.xml";
                var xmlCommentUtilFile = "TourManagement.Utility.xml";
                var xmlCommentFullPath = Path.Combine(AppContext.BaseDirectory, xmlCommentFile);
                var xmlCommentDtoFullPath = Path.Combine(AppContext.BaseDirectory, xmlCommentDtoFile);
                var xmlCommentUtilFullPath = Path.Combine(AppContext.BaseDirectory, xmlCommentUtilFile);
                setupAction.IncludeXmlComments(xmlCommentFullPath);
                setupAction.IncludeXmlComments(xmlCommentDtoFullPath);
                setupAction.IncludeXmlComments(xmlCommentUtilFullPath);

            });


            services.Configure<ApiBehaviorOptions>(options =>
            {
                options.InvalidModelStateResponseFactory = actionContext =>
                {
                    var actionExecutingContext =
                        actionContext as Microsoft.AspNetCore.Mvc.Filters.ActionExecutingContext;

                    // if there are modelstate errors & all keys were correctly
                    // found/parsed we're dealing with validation errors
                    if (actionContext.ModelState.ErrorCount > 0
                        && actionExecutingContext?.ActionArguments.Count == actionContext.ActionDescriptor.Parameters.Count)
                    {
                        return new UnprocessableEntityObjectResult(actionContext.ModelState);
                    }

                    // if one of the keys wasn't correctly found / couldn't be parsed
                    // we're dealing with null/unparsable input
                    return new BadRequestObjectResult(actionContext.ModelState);
                };
            });

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory, TourManagementContext context)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();                
            }
            else
            {
                //TODO::
                app.UseExceptionHandler(appBuilder =>
                {
                    appBuilder.Run(async (ctx) =>
                    {
                        var exceptionHandlerFeature = ctx.Features.Get<IExceptionHandlerFeature>();
                        if (exceptionHandlerFeature != null)
                        {
                            var logger = loggerFactory.CreateLogger("Global exception logger");
                            logger.LogError(500,
                                exceptionHandlerFeature.Error,
                                exceptionHandlerFeature.Error.Message);
                        }

                        ctx.Response.StatusCode = 500;
                        await ctx.Response.WriteAsync("An unexpected fault happened. Try again later.");
                    });
                });

                app.UseHsts();
            }

            AutoMapper.Mapper.Initialize(config =>
            {

                config.CreateMap<Band, BandDto>();
                config.CreateMap<BandForCreation, Band>();
                config.CreateMap<BandForUpdate, Band>();

            });

            // Enable CORS
            app.UseCors("AllowAllOriginsHeadersAndMethods");

            app.UseHttpsRedirection();

            app.UseSwagger();

            app.UseSwaggerUI(setupAction =>
            {
                setupAction.SwaggerEndpoint("/swagger/TourManagementAPISpecification/swagger.json", "Tourmanagement API");
                setupAction.RoutePrefix = "";
                //setupAction.OAuthClientId("swaggerui");
                //setupAction.OAuth2RedirectUrl("https://localhost:44390/oauth2-redirect.html");
                setupAction.DocExpansion(DocExpansion.None);
                setupAction.EnableDeepLinking();
                setupAction.DisplayOperationId();
            });

            app.UseMvc();

            //Add the seed data to the database.
            context.EnsureSeedDataForContext();

        }
    }
}
