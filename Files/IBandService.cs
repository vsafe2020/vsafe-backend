﻿using System;
using System.Threading.Tasks;
using TourManagement.Dto;
using TourManagement.Domain.Entities;

namespace TourManagement.Service
{
    public interface IBandService : IService<Band, Guid>
    {

    }
}
