﻿using System;

namespace TourManagement.Dtos
{
    /// <summary>
    /// Show Model
    /// </summary>
    public class Show : ShowAbstractBase
    {
        /// <summary>
        /// Unique Identifier for the show.
        /// </summary>
        public Guid ShowId { get; set; }      
    }

    /// <summary>
    /// Show Model
    /// </summary>
    public class ShowWithCountryVenue 
    {
        /// <summary>
        /// Unique Identifier for the show.
        /// </summary>
        public Guid ShowId { get; set; }

        /// <summary>
        /// Venue for the show.
        /// </summary>
        public string Venue { get; set; }

        /// <summary>
        /// Country for the show.
        /// </summary>
        public string Country { get; set; }
    }
}
 