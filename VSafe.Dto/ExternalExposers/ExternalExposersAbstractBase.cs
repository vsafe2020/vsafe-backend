using System;

namespace VSafe.Dto
{
    public abstract class ExternalExposersAbstractBase
    {
        /// <summary>
        /// ExternalExposers Id.
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// ExternalExposers Name.
        /// </summary>
        public string Name { get; set; }

    }
}
