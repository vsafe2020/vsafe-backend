using System;

namespace VSafe.Dto
{
    public abstract class EmployeeExposersAbstractBase
    {
        /// <summary>
        /// EmployeeExposers Id.
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// EmployeeExposers Name.
        /// </summary>
        public string Name { get; set; }

    }
}
