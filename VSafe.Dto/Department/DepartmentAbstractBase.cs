using System;

namespace VSafe.Dto
{
    public abstract class DepartmentAbstractBase
    {
        /// <summary>
        /// Department Id.
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Department Name.
        /// </summary>
        public string Name { get; set; }

    }
}
