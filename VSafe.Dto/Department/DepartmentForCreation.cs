using System;
using System.ComponentModel.DataAnnotations;

namespace VSafe.Dto
{
    /// <summary>
    /// Department Create DTO
    /// </summary>
    public class DepartmentForCreation : DepartmentAbstractBase
    {
        /// <summary>
        /// AccountId
        /// </summary>
        public Guid AccountId { get; set; }
    }
}
