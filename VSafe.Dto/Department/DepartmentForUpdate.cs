using System;
using System.ComponentModel.DataAnnotations;

namespace VSafe.Dto
{
    /// <summary>
    /// Department Update Model.
    /// </summary>
    public class DepartmentForUpdate : DepartmentAbstractBase
    {
        public Guid? DepartmentHead { get; set; }
    }
}
