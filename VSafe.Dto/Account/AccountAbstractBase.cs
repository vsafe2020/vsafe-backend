using System;

namespace VSafe.Dto
{
    public abstract class AccountAbstractBase
    {
        /// <summary>
        /// Account Id.
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Account Name.
        /// </summary>
        public string Name { get; set; }

    }
}
