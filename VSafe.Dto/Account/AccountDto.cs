using System;

namespace VSafe.Dto
{
    /// <summary>
    /// Account Model
    /// </summary>
    public class AccountDto : AccountAbstractBase
    {
        
        public string Address { get; set; }

        public string Address2 { get; set; }

        public string City { get; set; }

        public string Country { get; set; }

        public string State { get; set; }

        public Guid TimezoneId { get; set; }

        public string DisplayName { get; set; }

        public string PostalCode { get; set; }

        public DateTime ExpirationDate { get; set; }

        public double Latitude { get; set; }

        public double Longitude { get; set; }

        public Guid? PrimaryContactId { get; set; }
    }

}
 