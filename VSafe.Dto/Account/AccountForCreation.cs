using System;
using System.ComponentModel.DataAnnotations;

namespace VSafe.Dto
{
    public class AccountForCreation : AccountAbstractBase
    {

        /// <summary>
        /// Account Email
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Account Website
        /// </summary>
        public string Website { get; set; }

        /// <summary>
        /// Account CompanyLogo
        /// </summary>
        public string CompanyLogo { get; set; }

        /// <summary>
        /// Account PhoneNo1
        /// </summary>
        public string PhoneNo1 { get; set; }

        /// <summary>
        /// Account PhoneNo2
        /// </summary>
        public string PhoneNo2 { get; set; }

        /// <summary>
        /// Account Address
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// Account Address2
        /// </summary>
        public string Address2 { get; set; }

        /// <summary>
        /// Account City
        /// </summary>
        public string City { get; set; }

        /// <summary>
        /// Account Country
        /// </summary>
        public string Country { get; set; }

        /// <summary>
        /// Account State
        /// </summary>
        public string State { get; set; }

        /// <summary>
        /// Account PostalCode
        /// </summary>
        public string PostalCode { get; set; }

        /// <summary>
        /// Account Latitude
        /// </summary>
        public double Latitude { get; set; }

        /// <summary>
        /// Account Longitude
        /// </summary>
        public double Longitude { get; set; }

        /// <summary>
        /// Account ExpirationDate
        /// </summary>
        public DateTime ExpirationDate { get; set; }


        /// <summary>
        /// Account Timezone
        /// </summary>
        public Guid TimeZoneId { get; set; }

        /// <summary>
        /// Primary Contact ID
        /// </summary>
        public Guid? PrimaryContactId { get; set; }
    }
}
