using System;

namespace VSafe.Dto
{
    public abstract class LocationAbstractBase
    {
        /// <summary>
        /// Location Id.
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Location Name.
        /// </summary>
        public string Name { get; set; }

    }
}
