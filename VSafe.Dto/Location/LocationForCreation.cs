using System;
using System.ComponentModel.DataAnnotations;

namespace VSafe.Dto
{
    /// <summary>
    /// Location Create DTO
    /// </summary>
    public class LocationForCreation : LocationAbstractBase
    {
        /// <summary>
        /// Location Address
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// Location Address2
        /// </summary>
        public string Address2 { get; set; }

        /// <summary>
        /// Location City
        /// </summary>
        public string City { get; set; }

        /// <summary>
        /// Location Country
        /// </summary>
        public string Country { get; set; }

        /// <summary>
        /// Location State
        /// </summary>
        public string State { get; set; }

        /// <summary>
        /// Location PostalCode
        /// </summary>
        public string PostalCode { get; set; }

        /// <summary>
        /// Location Latitude
        /// </summary>
        public double Latitude { get; set; }

        /// <summary>
        /// Location Longitude
        /// </summary>
        public double Longitude { get; set; }

        /// <summary>
        /// AccountId
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        /// Account Timezone
        /// </summary>
        public Guid? TimezoneId { get; set; }
    }
}
