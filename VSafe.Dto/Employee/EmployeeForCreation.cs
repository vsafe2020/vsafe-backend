using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace VSafe.Dto
{
    /// <summary>
    /// Employee Create DTO
    /// </summary>
    public class EmployeeForCreation : EmployeeAbstractBase
    {
        /// <summary>
        /// Employee Organization Id
        /// </summary>
        public string EmployeeId { get; set; }

        /// <summary>
        /// Employee FirstName
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// Employee FirstName
        /// </summary>
        public string FName { get; set; }

        /// <summary>
        /// Employee Last Name
        /// </summary>
        public string LName { get; set; }

        /// <summary>
        /// Employee Role
        /// </summary>
        [Required]
        public bool IsAdmin { get; set; }

        /// <summary>
        /// Employee Language
        /// </summary>
        [Required]
        [DefaultValue("en")]
        public string Language { get; set; }

        /// <summary>
        /// Status
        /// </summary>
        [Required]
        public string Status { get; set; }

        /// <summary>
        /// LastAssesmentDate Date
        /// </summary>
        [Required]
        public DateTime LastAssesmentDate { get; set; }

        /// <summary>
        /// Location Id
        /// </summary>
        [Required]
        public Guid LocationId { get; set; }

        /// <summary>
        /// Department Id
        /// </summary>
        [Required]
        public Guid DepartmentId { get; set; }

        /// <summary>
        /// Account Id
        /// 
        /// </summary>
        public Guid AccountId { get; set; }
    }
}
