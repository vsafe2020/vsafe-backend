using System;
using System.ComponentModel.DataAnnotations;

namespace VSafe.Dto
{
    /// <summary>
    /// Employee Model
    /// </summary>
    public class EmployeeDto : EmployeeAbstractBase
    {
       
        [Required]
        [MaxLength(30)]
        [MinLength(8)]
        public string Password { get; set; }

        [Required]
        public string Role { get; set; }

        [Required]
        public Guid LocationId { get; set; }

        [Required]
        public Guid DepartmentId { get; set; }

    }

}
 