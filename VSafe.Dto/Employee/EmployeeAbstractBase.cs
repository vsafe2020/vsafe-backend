using System;

namespace VSafe.Dto
{
    public abstract class EmployeeAbstractBase
    {
        /// <summary>
        /// Employee Id.
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Employee Name.
        /// </summary>
        public string Name { get; set; }

    }
}
