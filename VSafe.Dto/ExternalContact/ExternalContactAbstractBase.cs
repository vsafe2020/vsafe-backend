using System;

namespace VSafe.Dto
{
    public abstract class ExternalContactAbstractBase
    {
        /// <summary>
        /// ExternalContact Id.
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// ExternalContact Name.
        /// </summary>
        public string Name { get; set; }

    }
}
