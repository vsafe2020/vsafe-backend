using System;
using System.ComponentModel.DataAnnotations;

namespace VSafe.Dto
{
    public class TimeZoneForCreation : TimeZoneAbstractBase
    {
        public string TimeZoneIDString { get; set; }

        public bool SupportsDST { get; set; }

        public string Region { get; set; }

    }
}
