using System;

namespace VSafe.Dto
{
    public abstract class TimeZoneAbstractBase
    {
        /// <summary>
        /// TimeZone Id.
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// TimeZone Name.
        /// </summary>
        public string Name { get; set; }

    }
}
