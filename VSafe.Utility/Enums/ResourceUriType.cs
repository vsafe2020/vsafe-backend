using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VSafe.Utility
{
    public enum ResourceUriType
    {
        PreviousPage,
        NextPage,
        Current
    }
}
