using System;
using System.Collections.Generic;
using VSafe.Domain;
using VSafe.Domain.Entities;
using VSafe.Dto;
using Microsoft.Extensions.Logging;

namespace VSafe.Service
{
    public class DepartmentService : ServiceBase<Department, Guid>, IDepartmentService
    {

        #region PRIVATE MEMBERS

        private readonly IDepartmentRepository _departmentRepository;

        #endregion


        #region CONSTRUCTOR

        public DepartmentService(IDepartmentRepository departmentRepository, ILogger<DepartmentService> logger) : base(departmentRepository, logger)
        {
            _departmentRepository = departmentRepository;
        }

        #endregion


        #region PUBLIC MEMBERS   


        #endregion


        #region OVERRIDDEN IMPLEMENTATION

        public override Dictionary<string, PropertyMappingValue> GetPropertyMapping()
        {
            return new Dictionary<string, PropertyMappingValue>(StringComparer.OrdinalIgnoreCase)
                    {
                        { "Id", new PropertyMappingValue(new List<string>() { "Id" } ) },
                        { "Name", new PropertyMappingValue(new List<string>() { "Name" } )},
                        { "UpdatedOn", new PropertyMappingValue(new List<string>() { "UpdatedOn" } )},
                    };
        }

        public override string GetDefaultOrderByColumn()
        {
            return "UpdatedOn desc";
        }

        public override string GetDefaultFieldsToSelect()
        {
            return "Id,Name";
        }

        #endregion
    }
}
