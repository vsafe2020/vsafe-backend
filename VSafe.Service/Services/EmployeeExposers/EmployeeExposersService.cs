using System;
using System.Collections.Generic;
using VSafe.Domain;
using VSafe.Domain.Entities;
using VSafe.Dto;
using Microsoft.Extensions.Logging;

namespace VSafe.Service
{
    public class EmployeeExposersService : ServiceBase<EmployeeExposer, Guid>, IEmployeeExposersService
    {

        #region PRIVATE MEMBERS

        private readonly IEmployeeExposersRepository _employeeexposersRepository;

        #endregion


        #region CONSTRUCTOR

        public EmployeeExposersService(IEmployeeExposersRepository employeeexposersRepository, ILogger<EmployeeExposersService> logger) : base(employeeexposersRepository, logger)
        {
            _employeeexposersRepository = employeeexposersRepository;
        }

        #endregion


        #region PUBLIC MEMBERS   


        #endregion


        #region OVERRIDDEN IMPLEMENTATION

        public override Dictionary<string, PropertyMappingValue> GetPropertyMapping()
        {
            return new Dictionary<string, PropertyMappingValue>(StringComparer.OrdinalIgnoreCase)
                    {
                        { "Id", new PropertyMappingValue(new List<string>() { "Id" } ) },
                        { "Name", new PropertyMappingValue(new List<string>() { "Name" } )},
                        { "UpdatedOn", new PropertyMappingValue(new List<string>() { "UpdatedOn" } )},
                    };
        }

        public override string GetDefaultOrderByColumn()
        {
            return "UpdatedOn";
        }

        public override string GetDefaultFieldsToSelect()
        {
            return "Id,Name";
        }

        #endregion
    }
}
