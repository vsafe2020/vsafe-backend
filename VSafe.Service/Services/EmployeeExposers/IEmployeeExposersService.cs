using System;
using System.Threading.Tasks;
using VSafe.Dto;
using VSafe.Domain.Entities;

namespace VSafe.Service
{
    public interface IEmployeeExposersService : IService<EmployeeExposer, Guid>
    {

    }
}
