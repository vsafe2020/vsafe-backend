using System;
using System.Collections.Generic;
using VSafe.Domain;
using VSafe.Domain.Entities;
using VSafe.Dto;
using Microsoft.Extensions.Logging;

namespace VSafe.Service
{
    public class EmployeeService : ServiceBase<Employee, Guid>, IEmployeeService
    {

        #region PRIVATE MEMBERS

        private readonly IEmployeeRepository _employeeRepository;

        #endregion


        #region CONSTRUCTOR

        public EmployeeService(IEmployeeRepository employeeRepository, ILogger<EmployeeService> logger) : base(employeeRepository, logger)
        {
            _employeeRepository = employeeRepository;
        }

        #endregion


        #region PUBLIC MEMBERS   


        #endregion


        #region OVERRIDDEN IMPLEMENTATION

        public override Dictionary<string, PropertyMappingValue> GetPropertyMapping()
        {
            return new Dictionary<string, PropertyMappingValue>(StringComparer.OrdinalIgnoreCase)
                    {
                        { "Id", new PropertyMappingValue(new List<string>() { "Id" } ) },
                        { "Name", new PropertyMappingValue(new List<string>() { "Name" } )},
                        { "UpdatedOn", new PropertyMappingValue(new List<string>() { "UpdatedOn" } )},
                    };
        }

        public override string GetDefaultOrderByColumn()
        {
            return "UpdatedOn desc";
        }

        public override string GetDefaultFieldsToSelect()
        {
            return "Id,Name";
        }

        #endregion
    }
}
