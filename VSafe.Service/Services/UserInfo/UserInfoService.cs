using Microsoft.AspNetCore.Http;
using System;
using System.Linq;
using VSafe.Domain;

namespace VSafe.Service
{
    public class UserInfoService : IUserInfoService
    {
        private readonly IHttpContextAccessor _httpContextAccessor;

        public string UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Role { get; set; }

        public Guid AccountId { get; set; }

        public UserInfoService(IHttpContextAccessor httpContextAccessor)
        {
            // service is scoped, created once for each request => we only need
            // to fetch the info in the constructor
            _httpContextAccessor = httpContextAccessor
                ?? throw new ArgumentNullException(nameof(httpContextAccessor));

            var currentContext = _httpContextAccessor.HttpContext;
            if (currentContext == null || !currentContext.User.Identity.IsAuthenticated)
            {
                return;
            }

            UserId = (currentContext.User.Claims.FirstOrDefault(c => c.Type == "sub")?.Value) ?? "n/a";
            FirstName = (currentContext.User.Claims.FirstOrDefault(c => c.Type == "given_name")?.Value) ?? "n/a";
            LastName = (currentContext.User.Claims.FirstOrDefault(c => c.Type == "family_name")?.Value) ?? "n/a";
            Role = currentContext.User.Claims.FirstOrDefault(c => c.Type == "role")?.Value;
            AccountId = new Guid("613F262E-2B7F-40BC-0016-08D874EFC475");
        }
    }

}