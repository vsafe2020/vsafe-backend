using System;
using System.Collections.Generic;
using VSafe.Domain;
using VSafe.Domain.Entities;
using VSafe.Dto;
using Microsoft.Extensions.Logging;

namespace VSafe.Service
{
    public class LocationService : ServiceBase<Location, Guid>, ILocationService
    {

        #region PRIVATE MEMBERS

        private readonly ILocationRepository _locationRepository;

        #endregion


        #region CONSTRUCTOR

        public LocationService(ILocationRepository locationRepository, ILogger<LocationService> logger) : base(locationRepository, logger)
        {
            _locationRepository = locationRepository;
        }

        #endregion


        #region PUBLIC MEMBERS   


        #endregion


        #region OVERRIDDEN IMPLEMENTATION

        public override Dictionary<string, PropertyMappingValue> GetPropertyMapping()
        {
            return new Dictionary<string, PropertyMappingValue>(StringComparer.OrdinalIgnoreCase)
                    {
                        { "Id", new PropertyMappingValue(new List<string>() { "Id" } ) },
                        { "Name", new PropertyMappingValue(new List<string>() { "Name" } )},
                        { "UpdatedOn", new PropertyMappingValue(new List<string>() { "UpdatedOn" } )},
                    };
        }

        public override string GetDefaultOrderByColumn()
        {
            return "UpdatedOn";
        }

        public override string GetDefaultFieldsToSelect()
        {
            return "Id,Name";
        }

        #endregion
    }
}
