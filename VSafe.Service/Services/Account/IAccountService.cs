using System;
using System.Threading.Tasks;
using VSafe.Dto;
using VSafe.Domain.Entities;

namespace VSafe.Service
{
    public interface IAccountService : IService<Account, Guid>
    {

    }
}
