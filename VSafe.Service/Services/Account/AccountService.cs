using System;
using System.Collections.Generic;
using VSafe.Domain;
using VSafe.Domain.Entities;
using VSafe.Dto;
using Microsoft.Extensions.Logging;

namespace VSafe.Service
{
    public class AccountService : ServiceBase<Account, Guid>, IAccountService
    {

        #region PRIVATE MEMBERS

        private readonly IAccountRepository _accountRepository;

        #endregion


        #region CONSTRUCTOR

        public AccountService(IAccountRepository accountRepository, ILogger<AccountService> logger) : base(accountRepository, logger)
        {
            _accountRepository = accountRepository;
        }

        #endregion


        #region PUBLIC MEMBERS   


        #endregion


        #region OVERRIDDEN IMPLEMENTATION

        public override Dictionary<string, PropertyMappingValue> GetPropertyMapping()
        {
            return new Dictionary<string, PropertyMappingValue>(StringComparer.OrdinalIgnoreCase)
                    {
                        { "Id", new PropertyMappingValue(new List<string>() { "Id" } ) },
                        { "Name", new PropertyMappingValue(new List<string>() { "Name" } )},
                        { "UpdatedOn", new PropertyMappingValue(new List<string>() { "UpdatedOn" } )},
                        { "TimeZoneId", new PropertyMappingValue(new List<string>() { "TimeZoneId" } )},
                        { "DisplayName", new PropertyMappingValue(new List<string>() { "DisplayName" } )},
                        { "PrimaryContactId", new PropertyMappingValue(new List<string>() { "PrimaryContactId" } )},
                    };
        }

        public override string GetDefaultOrderByColumn()
        {
            return "UpdatedOn";
        }

        public override string GetDefaultFieldsToSelect()
        {
            return "Id,Name";
        }

        #endregion
    }
}
