using System;
using VSafe.Domain.Entities;

namespace VSafe.Domain
{
    public interface ILocationRepository : IRepository<Location, Guid>
    {

    }
}
