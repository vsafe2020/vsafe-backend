using System;
using VSafe.Domain.Entities;

namespace VSafe.Domain
{
    public interface IEmployeeExposersRepository : IRepository<EmployeeExposer, Guid>
    {

    }
}
