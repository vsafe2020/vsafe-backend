using System;
using VSafe.Domain.Entities;

namespace VSafe.Domain
{
    public interface IDepartmentRepository : IRepository<Department, Guid>
    {

    }
}
