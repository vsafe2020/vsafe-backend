using System;
using VSafe.Domain.Entities;

namespace VSafe.Domain
{
    public interface ITimeZoneRepository : IRepository<Entities.TimeZone, Guid>
    {

    }
}
