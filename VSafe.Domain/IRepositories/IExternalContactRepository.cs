using System;
using VSafe.Domain.Entities;

namespace VSafe.Domain
{
    public interface IExternalContactRepository : IRepository<ExternalContact, Guid>
    {

    }
}
