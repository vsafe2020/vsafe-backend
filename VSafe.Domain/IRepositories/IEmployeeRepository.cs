using System;
using VSafe.Domain.Entities;

namespace VSafe.Domain
{
    public interface IEmployeeRepository : IRepository<Employee, Guid>
    {

    }
}
