using System;
using VSafe.Domain.Entities;

namespace VSafe.Domain
{
    public interface IAccountRepository : IRepository<Account, Guid>
    {

    }
}
