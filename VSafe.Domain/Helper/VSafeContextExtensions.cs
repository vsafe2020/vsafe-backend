using System;
using System.Collections.Generic;
using VSafe.Domain.Entities;

namespace VSafe.Domain   
{
    public static class VSafeContextExtensions
    {
        public static void EnsureSeedDataForContext(this VSafeContext context)
        {
            // first, clear the database.  This ensures we can always start 
            // fresh with each demo.  Not advised for production environments, obviously :-)

            //context.Locations.RemoveRange(context.Locations);
            //context.SaveChanges();

            //var locations = new List<Location>()
            //{
            //    new Location()
            //    {
            //        Id = new Guid("25320c5e-f58a-4b1f-b63a-8ee07a840bdf"),
            //        Name = "Queens of the Stone Age",
            //        CreatedBy = "system",
            //        CreatedOn = DateTime.UtcNow
            //    },
            //    new Location()
            //    {
            //        Id = new Guid("83b126b9-d7bf-4f50-96dc-860884155f8b"),
            //        Name = "Nick Cave and the Bad Seeds",
            //        CreatedBy = "system",
            //        CreatedOn = DateTime.UtcNow
            //    }
            //};

            //context.Locations.AddRange(locations);
            //context.SaveChanges();
        }
    }
}
