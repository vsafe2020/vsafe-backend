using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace VSafe.Domain.Entities
{
    /// <summary>
    /// EmployeeExposers Entity
    /// </summary>
    public class EmployeeExposer : AuditableEntity
    {
        /// <summary>
        /// Unique Indentifier
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }

        /// <summary>
        /// Contact Employee Id
        /// </summary>
        [Required]
        [ForeignKey("ContactEmployee")]
        public Guid ContactId { get; set; }

        /// <summary>
        /// RSSI
        /// </summary>
        [Required]
        public double RSSI { get; set; }

        /// <summary>
        /// StartContactDate
        /// </summary>
        [Required]
        public DateTime StartContactDate { get; set; }

        /// <summary>
        /// EndContactDate
        /// </summary>
        [Required]
        public DateTime EndContactDate { get; set; }


        #region Foreign Key

        /// <summary>
        /// Employee AccountId
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        /// Employee Account
        /// </summary>
        public Account Account { get; set; }

        /// <summary>
        /// Employee Id
        /// </summary>
        [Required]
        public Guid EmployeeId { get; set; }

        /// <summary>
        /// Employee Info
        /// </summary>
        public Employee Employee { get; set; }

        /// <summary>
        /// ContactEmployee Info
        /// </summary>
        public Employee ContactEmployee { get; set; }

        #endregion
    }
}
