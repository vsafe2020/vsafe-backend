using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace VSafe.Domain.Entities
{
    /// <summary>
    /// ExternalExposers Entity
    /// </summary>
    public class ExternalExposer : AuditableEntity
    {
        /// <summary>
        /// Unique Identifier
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }

        /// <summary>
        /// Contact Employee Id
        /// </summary>
        [Required]
        public Guid ExternalContactId { get; set; }

        /// <summary>
        /// RSSI
        /// </summary>
        [Required]
        public double RSSI { get; set; }

        /// <summary>
        /// StartContactDate
        /// </summary>
        [Required]
        public DateTime StartContactDate { get; set; }

        /// <summary>
        /// EndContactDate
        /// </summary>
        [Required]
        public DateTime EndContactDate { get; set; }

        #region Foreign Key

        /// <summary>
        /// Employee Id
        /// </summary>
        [Required]
        public Guid EmployeeId { get; set; }

        /// <summary>
        /// Employee Info
        /// </summary>
        public Employee Employee { get; set; }

        /// <summary>
        /// Employee Info
        /// </summary>
        public ExternalContact ExternalContact { get; set; }

        /// <summary>
        /// Employee AccountId
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        /// Employee Account
        /// </summary>
        public Account Account { get; set; }

        #endregion
    }
}
