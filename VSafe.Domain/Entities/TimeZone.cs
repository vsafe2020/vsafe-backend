using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace VSafe.Domain.Entities
{
    /// <summary>
    /// Timezone Entity
    /// </summary>
    public class TimeZone : AuditableEntity
    {
        /// <summary>
        /// Timezone Unique Indetifier.
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }

        /// <summary>
        /// Timezone Name
        /// </summary>
        [Required]
        public string Name { get; set; }

        /// <summary>
        /// TimezoneIDString
        /// </summary>
        [Required]
        public string TimeZoneIDString { get; set; }

        /// <summary>
        /// Timezone DST Info
        /// </summary>
        [Required]
        public bool SupportsDST { get; set; }

        /// <summary>
        /// Timezone Region
        /// </summary>
        [Required]
        public string Region { get; set; }
       
    }
}
