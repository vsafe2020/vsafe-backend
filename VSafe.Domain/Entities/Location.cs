using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace VSafe.Domain.Entities
{
    /// <summary>
    /// Location Entity
    /// </summary>
    public class Location : AuditableEntity
    {
        /// <summary>
        /// Unique Identifier for Location 
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }

        /// <summary>
        /// Location Name
        /// </summary>
        [Required]
        [MaxLength(250)]
        public string Name { get; set; }

        /// <summary>
        /// Location Address
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// Location Address2
        /// </summary>
        public string Address2 { get; set; }

        /// <summary>
        /// Location City
        /// </summary>
        public string City { get; set; }

        /// <summary>
        /// Location Country
        /// </summary>
        public string Country { get; set; }

        /// <summary>
        /// Location State
        /// </summary>
        public string State { get; set; }

        /// <summary>
        /// Location PostalCode
        /// </summary>
        public string PostalCode { get; set; }

        /// <summary>
        /// Location Latitude
        /// </summary>
        public double Latitude { get; set; }

        /// <summary>
        /// Location Longitude
        /// </summary>
        public double Longitude { get; set; }


        #region Foreign Key

        /// <summary>
        /// Department AccountId
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        /// Department Account
        /// </summary>
        public Account Account { get; set; }

        /// <summary>
        /// Account Timezone
        /// </summary>
        public Guid? TimezoneId { get; set; }

        /// <summary>
        /// Timezone Info
        /// </summary>
        public TimeZone TimeZone { get; set; }

      

        #endregion
    }
}
