using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace VSafe.Domain.Entities
{
    /// <summary>
    /// Employee Entity
    /// </summary>
    public class Employee : AuditableEntity
    {
        /// <summary>
        /// Unique Identife for Employee
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }

        /// <summary>
        /// Employee Email
        /// </summary>
        [Required]
        public string Name { get; set; }

        /// <summary>
        /// Employee Organization Id
        /// </summary>
        public string EmployeeId { get; set; }

        /// <summary>
        /// Employee FirstName
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// Employee FirstName
        /// </summary>
        public string FName { get; set; }

        /// <summary>
        /// Employee Last Name
        /// </summary>
        public string LName { get; set; }

        /// <summary>
        /// Employee Role
        /// </summary>
        [Required]
        public bool IsAdmin { get; set; }

        /// <summary>
        /// Employee Language
        /// </summary>
        [Required]
        [DefaultValue("en")]
        public string Language { get; set; }

        /// <summary>
        /// Status
        /// </summary>
        [Required]
        public string Status { get; set; }

        /// <summary>
        /// LastAssesmentDate Date
        /// </summary>
        [Required]
        public DateTime LastAssesmentDate { get; set; }

        #region Foreign Key

        /// <summary>
        /// Employee Department Id
        /// </summary>
        [Required]
        public Guid DepartmentId { get; set; }

        /// <summary>
        /// Employee Department
        /// </summary>
        public Department Department { get; set; }

        /// <summary>
        /// Employee AccountId
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        /// Employee Account
        /// </summary>
        public Account Account { get; set; }

        /// <summary>
        /// Location Id
        /// </summary>
        [Required]
        public Guid LocationId { get; set; }

        /// <summary>
        /// Employee Location Info
        /// </summary>
        public Location Location { get; set; }

        #endregion


    }
}
