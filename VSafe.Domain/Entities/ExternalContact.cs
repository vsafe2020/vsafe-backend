using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace VSafe.Domain.Entities
{
    /// <summary>
    /// External Contact Entity
    /// </summary>
    public class ExternalContact : AuditableEntity
    {
        /// <summary>
        /// Unique identifier
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }

        /// <summary>
        /// Name
        /// </summary>
        [Required]
        public string Name { get; set; }

        /// <summary>
        /// Email
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Phone
        /// </summary>
        public string Phone { get; set; }

        /// <summary>
        /// Company
        /// </summary>
        public string Company { get; set; }

        /// <summary>
        /// Designation
        /// </summary>
        public string Designation { get; set; }

        #region Foreign Key

        /// <summary>
        /// Department AccountId
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        /// Department Account
        /// </summary>
        public Account Account { get; set; }

        #endregion

    }
}
