using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace VSafe.Domain.Entities
{
    /// <summary>
    /// Department Entity
    /// </summary>
    public class Department : AuditableEntity
    {
        /// <summary>
        /// Unique Indentifier for Department
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }

        /// <summary>
        /// Department Name
        /// </summary>
        [Required]
        [MaxLength(250)]
        public string Name { get; set; }

        #region Foreign Key

        /// <summary>
        /// DepartmentHead ID
        /// </summary>
        [ForeignKey("Employee")]
        public Guid? DepartmentHead { get; set; }

        /// <summary>
        /// DepartmentHead Info
        /// </summary>
        public Employee Employee { get; set; }

        /// <summary>
        /// Department AccountId
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        /// Department Account
        /// </summary>
        public Account Account { get; set; }

        /// <summary>
        /// Employee List
        /// </summary>
        //public List<Employee> Employees { get; set; }
        #endregion
    }
}
