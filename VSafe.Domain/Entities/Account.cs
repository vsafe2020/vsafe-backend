using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace VSafe.Domain.Entities
{

    /// <summary>
    /// Account Entity
    /// </summary>
    public class Account : AuditableEntity
    {
        /// <summary>
        /// Unique Indentifier for the account
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }

        /// <summary>
        /// Account Name
        /// </summary>
        [Required]
        public string Name { get; set; }

        /// <summary>
        /// Account Email
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Account Website
        /// </summary>
        public string Website { get; set; }

        /// <summary>
        /// Account CompanyLogo
        /// </summary>
        public string CompanyLogo { get; set; }

        /// <summary>
        /// Account PhoneNo1
        /// </summary>
        public string PhoneNo1 { get; set; }

        /// <summary>
        /// Account PhoneNo2
        /// </summary>
        public string PhoneNo2 { get; set; }

        /// <summary>
        /// Account Address
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// Account Address2
        /// </summary>
        public string Address2 { get; set; }

        /// <summary>
        /// Account City
        /// </summary>
        public string City { get; set; }

        /// <summary>
        /// Account Country
        /// </summary>
        public string Country { get; set; }

        /// <summary>
        /// Account State
        /// </summary>
        public string State { get; set; }

        /// <summary>
        /// Account PostalCode
        /// </summary>
        public string PostalCode { get; set; }

        /// <summary>
        /// Account Latitude
        /// </summary>
        public double Latitude { get; set; }

        /// <summary>
        /// Account Longitude
        /// </summary>
        public double Longitude { get; set; }

        /// <summary>
        /// Account ExpirationDate
        /// </summary>
        public DateTime ExpirationDate { get; set; }

        #region Foreign Key

        /// <summary>
        /// Account Timezone
        /// </summary>
        public Guid TimeZoneId { get; set; }

        /// <summary>
        /// Timezone Info
        /// </summary>
        public TimeZone TimeZone { get; set; }

        /// <summary>
        /// Primary Contact ID
        /// </summary>
        [ForeignKey("Employee")]
        public Guid? PrimaryContactId { get; set; }

        /// <summary>
        /// Primary Contact Info
        /// </summary>
        public Employee Employee { get; set; }

        /// <summary>
        /// Department List
        /// </summary>
        public List<Department> Departments { get; set; }

        /// <summary>
        /// Location List
        /// </summary>
        public List<Location> Locations { get; set; }

        #endregion



    }
}
