using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using VSafe.Domain.Entities;

namespace VSafe.Domain
{
    /// <summary>
    /// VSafe DBContext
    /// </summary>
    public class VSafeContext : DbContext
    {

        #region PUBLIC MEMBERS

        /// <summary>
        /// ExternalExposerss
        /// </summary>
        //public DbSet<ExternalExposer> ExternalExposers { get; set; }

        /// <summary>
        /// ExternalContacts
        /// </summary>
        //public DbSet<ExternalContact> ExternalContacts { get; set; }

        /// <summary>
        /// EmployeeExposerss
        /// </summary>
        //public DbSet<EmployeeExposer> EmployeeExposers { get; set; }

        /// <summary>
        /// Employees
        /// </summary>
        public DbSet<Employee> Employees { get; set; }

        /// <summary>
        /// TimeZones
        /// </summary>
        public DbSet<Entities.TimeZone> TimeZones { get; set; }

        /// <summary>
        /// Departments
        /// </summary>
        public DbSet<Department> Departments { get; set; }

        /// <summary>
        /// Accounts
        /// </summary>
        public DbSet<Account> Accounts { get; set; }

        /// <summary>
        /// Locations
        /// </summary>
        public DbSet<Location> Locations { get; set; }


        private readonly IUserInfoService _userInfoService;

        #endregion

        #region CONSTRUCTOR

        /// <summary>
        /// VSafeContext constructor
        /// </summary>
        /// <param name="options"></param>
        /// <param name="userInfoService"></param>
        public VSafeContext(DbContextOptions<VSafeContext> options, IUserInfoService userInfoService)
         : base(options)
        {
            // userInfoService is a required argument
            _userInfoService = userInfoService ?? throw new ArgumentNullException(nameof(userInfoService));
        }

        #endregion


        /// <summary>
        /// Save Chnages to the DB
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <returns>No Of recrods affected</returns>
        public override Task<int> SaveChangesAsync(CancellationToken cancellationToken = default(CancellationToken))
        {
            // get added or updated entries
            var addedOrUpdatedEntries = ChangeTracker.Entries()
                    .Where(x => (x.State == EntityState.Added || x.State == EntityState.Modified));

            // fill out the audit fields
            foreach (var entry in addedOrUpdatedEntries)
            {
                var entity = entry.Entity as AuditableEntity;

                if (entry.State == EntityState.Added)
                {
                    if (_userInfoService.UserId == null)
                    {
                        entity.CreatedBy = "system";
                    }
                    else
                    {
                        entity.CreatedBy = _userInfoService.UserId;
                    }
                    entity.CreatedOn = DateTime.UtcNow;
                }

                entity.UpdatedBy = _userInfoService.UserId;
                entity.UpdatedOn = DateTime.UtcNow;
            }

            return base.SaveChangesAsync(cancellationToken);
        }
    }
}
