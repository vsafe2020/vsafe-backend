﻿// <auto-generated />
using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using VSafe.Domain;

namespace VSafe.Domain.Migrations
{
    [DbContext(typeof(VSafeContext))]
    partial class VSafeContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "2.1.14-servicing-32113")
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("VSafe.Domain.Entities.Account", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Address");

                    b.Property<string>("Address2");

                    b.Property<string>("City");

                    b.Property<string>("CompanyLogo");

                    b.Property<string>("Country");

                    b.Property<string>("CreatedBy")
                        .IsRequired();

                    b.Property<DateTime>("CreatedOn");

                    b.Property<string>("Email");

                    b.Property<DateTime>("ExpirationDate");

                    b.Property<double>("Latitude");

                    b.Property<double>("Longitude");

                    b.Property<string>("Name")
                        .IsRequired();

                    b.Property<string>("PhoneNo1");

                    b.Property<string>("PhoneNo2");

                    b.Property<string>("PostalCode");

                    b.Property<Guid?>("PrimaryContactId");

                    b.Property<string>("State");

                    b.Property<Guid>("TimeZoneId");

                    b.Property<string>("UpdatedBy");

                    b.Property<DateTime>("UpdatedOn");

                    b.Property<string>("Website");

                    b.HasKey("Id");

                    b.HasIndex("PrimaryContactId")
                        .IsUnique()
                        .HasFilter("[PrimaryContactId] IS NOT NULL");

                    b.HasIndex("TimeZoneId");

                    b.ToTable("Accounts");
                });

            modelBuilder.Entity("VSafe.Domain.Entities.Department", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<Guid>("AccountId");

                    b.Property<string>("CreatedBy")
                        .IsRequired();

                    b.Property<DateTime>("CreatedOn");

                    b.Property<Guid?>("DepartmentHead");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(250);

                    b.Property<string>("UpdatedBy");

                    b.Property<DateTime>("UpdatedOn");

                    b.HasKey("Id");

                    b.HasIndex("AccountId");

                    b.HasIndex("DepartmentHead")
                        .IsUnique()
                        .HasFilter("[DepartmentHead] IS NOT NULL");

                    b.ToTable("Departments");
                });

            modelBuilder.Entity("VSafe.Domain.Entities.Employee", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<Guid>("AccountId");

                    b.Property<string>("CreatedBy")
                        .IsRequired();

                    b.Property<DateTime>("CreatedOn");

                    b.Property<Guid>("DepartmentId");

                    b.Property<string>("EmployeeId");

                    b.Property<string>("FName");

                    b.Property<bool>("IsAdmin");

                    b.Property<string>("LName");

                    b.Property<string>("Language")
                        .IsRequired();

                    b.Property<DateTime>("LastAssesmentDate");

                    b.Property<Guid>("LocationId");

                    b.Property<string>("Name")
                        .IsRequired();

                    b.Property<string>("Password");

                    b.Property<string>("Status")
                        .IsRequired();

                    b.Property<string>("UpdatedBy");

                    b.Property<DateTime>("UpdatedOn");

                    b.HasKey("Id");

                    b.HasIndex("LocationId");

                    b.ToTable("Employees");
                });

            modelBuilder.Entity("VSafe.Domain.Entities.Location", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<Guid>("AccountId");

                    b.Property<string>("Address");

                    b.Property<string>("Address2");

                    b.Property<string>("City");

                    b.Property<string>("Country");

                    b.Property<string>("CreatedBy")
                        .IsRequired();

                    b.Property<DateTime>("CreatedOn");

                    b.Property<double>("Latitude");

                    b.Property<double>("Longitude");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(250);

                    b.Property<string>("PostalCode");

                    b.Property<string>("State");

                    b.Property<Guid?>("TimezoneId");

                    b.Property<string>("UpdatedBy");

                    b.Property<DateTime>("UpdatedOn");

                    b.HasKey("Id");

                    b.HasIndex("AccountId");

                    b.HasIndex("TimezoneId");

                    b.ToTable("Locations");
                });

            modelBuilder.Entity("VSafe.Domain.Entities.TimeZone", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("CreatedBy")
                        .IsRequired();

                    b.Property<DateTime>("CreatedOn");

                    b.Property<string>("Name")
                        .IsRequired();

                    b.Property<string>("Region")
                        .IsRequired();

                    b.Property<bool>("SupportsDST");

                    b.Property<string>("TimeZoneIDString")
                        .IsRequired();

                    b.Property<string>("UpdatedBy");

                    b.Property<DateTime>("UpdatedOn");

                    b.HasKey("Id");

                    b.ToTable("TimeZones");
                });

            modelBuilder.Entity("VSafe.Domain.Entities.Account", b =>
                {
                    b.HasOne("VSafe.Domain.Entities.Employee", "Employee")
                        .WithOne("Account")
                        .HasForeignKey("VSafe.Domain.Entities.Account", "PrimaryContactId");

                    b.HasOne("VSafe.Domain.Entities.TimeZone", "TimeZone")
                        .WithMany()
                        .HasForeignKey("TimeZoneId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("VSafe.Domain.Entities.Department", b =>
                {
                    b.HasOne("VSafe.Domain.Entities.Account", "Account")
                        .WithMany("Departments")
                        .HasForeignKey("AccountId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("VSafe.Domain.Entities.Employee", "Employee")
                        .WithOne("Department")
                        .HasForeignKey("VSafe.Domain.Entities.Department", "DepartmentHead");
                });

            modelBuilder.Entity("VSafe.Domain.Entities.Employee", b =>
                {
                    b.HasOne("VSafe.Domain.Entities.Location", "Location")
                        .WithMany()
                        .HasForeignKey("LocationId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("VSafe.Domain.Entities.Location", b =>
                {
                    b.HasOne("VSafe.Domain.Entities.Account", "Account")
                        .WithMany("Locations")
                        .HasForeignKey("AccountId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("VSafe.Domain.Entities.TimeZone", "TimeZone")
                        .WithMany()
                        .HasForeignKey("TimezoneId");
                });
#pragma warning restore 612, 618
        }
    }
}
