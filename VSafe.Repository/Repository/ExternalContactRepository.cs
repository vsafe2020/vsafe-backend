using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using VSafe.Domain;
using VSafe.Domain.Entities;
using Microsoft.Extensions.Logging;

namespace VSafe.Repository
{
    public class ExternalContactRepository : Repository<ExternalContact, Guid>, IExternalContactRepository
    {
        #region PRIVATE VARIABLE

        private readonly DbSet<ExternalContact> _dbset;
        private readonly VSafeContext _context;

        #endregion

        #region CONSTRUCTOR

        public ExternalContactRepository(VSafeContext context, ILogger<ExternalContactRepository> logger) : base(context, logger)
        {
            _context = context;
            _dbset = context.Set<ExternalContact>();
        }

        #endregion

        #region OVERRIDDEN IMPLEMENTATION

        public override string GetPrimaryKeyColumnName()
        {
            return "Id";
        }

        public override IQueryable<ExternalContact> GetFilteredEntities(bool bIsAsTrackable = false)
        {
            return _dbset;
        }

        #endregion

    }
}
