using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using VSafe.Domain;
using VSafe.Domain.Entities;
using Microsoft.Extensions.Logging;

namespace VSafe.Repository
{
    public class ExternalExposersRepository : Repository<ExternalExposer, Guid>, IExternalExposersRepository
    {
        #region PRIVATE VARIABLE

        private readonly DbSet<ExternalExposer> _dbset;
        private readonly VSafeContext _context;

        #endregion

        #region CONSTRUCTOR

        public ExternalExposersRepository(VSafeContext context, ILogger<ExternalExposersRepository> logger) : base(context, logger)
        {
            _context = context;
            _dbset = context.Set<ExternalExposer>();
        }

        #endregion

        #region OVERRIDDEN IMPLEMENTATION

        public override string GetPrimaryKeyColumnName()
        {
            return "Id";
        }

        public override IQueryable<ExternalExposer> GetFilteredEntities(bool bIsAsTrackable = false)
        {
            return _dbset;
        }

        #endregion

    }
}
