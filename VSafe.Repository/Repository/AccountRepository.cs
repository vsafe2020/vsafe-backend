using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using VSafe.Domain;
using VSafe.Domain.Entities;
using Microsoft.Extensions.Logging;

namespace VSafe.Repository
{
    public class AccountRepository : Repository<Account, Guid>, IAccountRepository
    {
        #region PRIVATE VARIABLE

        private readonly DbSet<Account> _dbset;
        private readonly VSafeContext _context;

        #endregion

        #region CONSTRUCTOR

        public AccountRepository(VSafeContext context, ILogger<AccountRepository> logger) : base(context, logger)
        {
            _context = context;
            _dbset = context.Set<Account>();
        }

        #endregion

        #region OVERRIDDEN IMPLEMENTATION

        public override string GetPrimaryKeyColumnName()
        {
            return "Id";
        }

        public override IQueryable<Account> GetFilteredEntities(bool bIsAsTrackable = false)
        {
            return _dbset;
        }

        #endregion

    }
}
