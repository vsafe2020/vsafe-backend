using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using VSafe.Domain;
using VSafe.Domain.Entities;
using Microsoft.Extensions.Logging;

namespace VSafe.Repository
{
    public class TimeZoneRepository : Repository<Domain.Entities.TimeZone, Guid>, ITimeZoneRepository
    {
        #region PRIVATE VARIABLE

        private readonly DbSet<Domain.Entities.TimeZone> _dbset;
        private readonly VSafeContext _context;

        #endregion

        #region CONSTRUCTOR

        public TimeZoneRepository(VSafeContext context, ILogger<TimeZoneRepository> logger) : base(context, logger)
        {
            _context = context;
            _dbset = context.Set<Domain.Entities.TimeZone>();
        }

        #endregion

        #region OVERRIDDEN IMPLEMENTATION

        public override string GetPrimaryKeyColumnName()
        {
            return "Id";
        }

        public override IQueryable<Domain.Entities.TimeZone> GetFilteredEntities(bool bIsAsTrackable = false)
        {
            return _dbset;
        }

        #endregion

    }
}
