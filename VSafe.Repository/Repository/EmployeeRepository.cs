using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using VSafe.Domain;
using VSafe.Domain.Entities;
using Microsoft.Extensions.Logging;

namespace VSafe.Repository
{
    public class EmployeeRepository : Repository<Employee, Guid>, IEmployeeRepository
    {
        #region PRIVATE VARIABLE

        private readonly DbSet<Employee> _dbset;
        private readonly VSafeContext _context;

        #endregion

        #region CONSTRUCTOR

        public EmployeeRepository(VSafeContext context, ILogger<EmployeeRepository> logger) : base(context, logger)
        {
            _context = context;
            _dbset = context.Set<Employee>();
        }

        #endregion

        #region PUBLUC METHODS


        #endregion

        #region OVERRIDDEN IMPLEMENTATION

        public override string GetPrimaryKeyColumnName()
        {
            return "Id";
        }

        public override IQueryable<Employee> GetFilteredEntities(bool bIsAsTrackable = false)
        {
            return _dbset;
        }

        #endregion

    }
}
