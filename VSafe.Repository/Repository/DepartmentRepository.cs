using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using VSafe.Domain;
using VSafe.Domain.Entities;
using Microsoft.Extensions.Logging;

namespace VSafe.Repository
{
    public class DepartmentRepository : Repository<Department, Guid>, IDepartmentRepository
    {
        #region PRIVATE VARIABLE

        private readonly DbSet<Department> _dbset;
        private readonly VSafeContext _context;

        #endregion

        #region CONSTRUCTOR

        public DepartmentRepository(VSafeContext context, ILogger<DepartmentRepository> logger) : base(context, logger)
        {
            _context = context;
            _dbset = context.Set<Department>();
        }

        #endregion

        #region OVERRIDDEN IMPLEMENTATION

        public override string GetPrimaryKeyColumnName()
        {
            return "Id";
        }

        public override IQueryable<Department> GetFilteredEntities(bool bIsAsTrackable = false)
        {
            return _dbset;
        }

        #endregion

    }
}
