using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using VSafe.Domain;
using VSafe.Domain.Entities;
using Microsoft.Extensions.Logging;

namespace VSafe.Repository
{
    public class EmployeeExposersRepository : Repository<EmployeeExposer, Guid>, IEmployeeExposersRepository
    {
        #region PRIVATE VARIABLE

        private readonly DbSet<EmployeeExposer> _dbset;
        private readonly VSafeContext _context;

        #endregion

        #region CONSTRUCTOR

        public EmployeeExposersRepository(VSafeContext context, ILogger<EmployeeExposersRepository> logger) : base(context, logger)
        {
            _context = context;
            _dbset = context.Set<EmployeeExposer>();
        }

        #endregion

        #region OVERRIDDEN IMPLEMENTATION

        public override string GetPrimaryKeyColumnName()
        {
            return "Id";
        }

        public override IQueryable<EmployeeExposer> GetFilteredEntities(bool bIsAsTrackable = false)
        {
            return _dbset;
        }

        #endregion

    }
}
