using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using VSafe.Domain;
using VSafe.Domain.Entities;
using Microsoft.Extensions.Logging;

namespace VSafe.Repository
{
    public class LocationRepository : Repository<Location, Guid>, ILocationRepository
    {
        #region PRIVATE VARIABLE

        private readonly DbSet<Location> _dbset;
        private readonly VSafeContext _context;

        #endregion

        #region CONSTRUCTOR

        public LocationRepository(VSafeContext context, ILogger<LocationRepository> logger) : base(context, logger)
        {
            _context = context;
            _dbset = context.Set<Location>();
        }

        #endregion

        #region OVERRIDDEN IMPLEMENTATION

        public override string GetPrimaryKeyColumnName()
        {
            return "Id";
        }

        public override IQueryable<Location> GetFilteredEntities(bool bIsAsTrackable = false)
        {
            return _dbset;
        }

        #endregion

    }
}
